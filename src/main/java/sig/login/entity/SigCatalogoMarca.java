package sig.login.entity;
// Generated 08-18-2019 03:27:04 PM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Gonzalez
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Table(name="sig_marca", catalog="sig115")
public class SigCatalogoMarca  implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "pk_marca",length = 4, unique = true, nullable = false)
    private int codigoMarca;
     
    @Column(name = "nombre", nullable = false, length = 25)
    private String nombre;
     
    @Column(name = "descripcion", nullable = true, length = 200)
    private String descripcion;
    
    @Column(name = "estado", nullable = false)
    private boolean estado;

    public SigCatalogoMarca(int codigoMarca) {
        this.codigoMarca = codigoMarca;
    }

    @Override
    public String toString() {
        String est;
        if(estado==true) est= "Activo";      
        else est= "Inactivo";
        return "Marca= " + nombre + ", Descripcion= " + descripcion + ", Estado= " + est + '.';
    }


}



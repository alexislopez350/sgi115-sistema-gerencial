/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.dao;

import sig.login.entity.SigCentro;
import java.util.List;
import sig.login.entity.SigIcono;

/**
 *
 * @author User
 */
public interface BuscadorDAO {
    //ICONO
    List<SigIcono> listIcono();
    //Centro
    List<SigCentro> listCentro();
}

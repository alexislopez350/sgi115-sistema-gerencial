/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.dao;

import sig.login.entity.SigCentro;
import java.util.List;
import lombok.Getter;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sig.login.entity.SigMenu;
import sig.login.entity.SigPermiso;
import sig.login.entity.SigRol;
import sig.login.entity.SigUbicacion;
import sig.login.entity.SigUsuario;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Administrador
 */
@Getter
@Transactional(value = "TMA")
@Service(value = "adminUsuarioDAO")
public class AdminUsuarioImplDAO extends HibernateDaoSupport implements AdminUsuarioDAO {

    @Autowired
    public AdminUsuarioImplDAO(@Qualifier(value = "SFA") SessionFactory sf) {
        super.setSessionFactory(sf);
    }
    
    @Autowired
    private CrudDAO crudDAO;

    @Override
    public List<SigRol> listRol() {
        return currentSession().createCriteria(SigRol.class)
                .add(Restrictions.eq("estado", true))
                .add(Restrictions.sqlRestriction("pk_rol > 1"))
                .addOrder(Order.asc("descripcion"))
                .list();
    }

    @Override
    public List<SigRol> listRolTodos() {
        return currentSession().createCriteria(SigRol.class)
                .addOrder(Order.asc("descripcion"))
                .add(Restrictions.sqlRestriction("pk_rol > 1"))
                .list();
    }

//    USUALMENTE DEBERIA HABER UN METODO LLAMADO: listCentroActivos que haga lo mismo que listRol()
        @Override
    public List<SigCentro> listCentro() {
        return currentSession().createCriteria(SigCentro.class)
                .add(Restrictions.sqlRestriction("pk_centro > 0"))
                .list();
    }
    
        @Override
    public List<SigCentro> listCentro2(int centro) {
        return currentSession().createCriteria(SigCentro.class)
                .add(Restrictions.sqlRestriction("pk_centro > 0"))
                .add(Restrictions.eq("pkIdCentro", centro))
                .list();
    }

    @Override
    public List<SigCentro> listCentroTodos() {
        return currentSession().createCriteria(SigCentro.class)
                .addOrder(Order.asc("pk_idCentro"))
                .add(Restrictions.sqlRestriction("pk_idCentro > 0"))
                .list();
    }

    @Override
    public List<SigPermiso> listMenu(int rol) {
        return currentSession().createCriteria(SigPermiso.class)
                .add(Restrictions.eq("id.fkRol", rol))
                .addOrder(Order.asc("orden"))
                .list();
    }

    @Override
    public List<SigMenu> listMenuAll() {
        return currentSession().createCriteria(SigMenu.class)
                .addOrder(Order.asc("nomVista"))
                .list();
    }

    @Override
    public List<SigUsuario> listUsuario() {
        return currentSession().createCriteria(SigUsuario.class)
                .add(Restrictions.ne("codigo","ROOT"))
                .addOrder(Order.asc("nombre"))
                .list();
    }
    
    @Override
    public List<SigUsuario> listaPorCentro(int centro) {
        return currentSession().createCriteria(SigUsuario.class)
                .addOrder(Order.asc("codigo"))
                .add(Restrictions.eq("sigCentro.pkIdCentro", centro))
                .list();
    }

    @Override
    public String consultarCodigo(String nomApell) {
        Date cal= new Date();
        //int anio=cal.getYear();
        String anio2=new SimpleDateFormat("yy").format(cal);
        String val = nomApell.toUpperCase() + anio2;
        String correlativo= String.valueOf(crudDAO.maxRubro("SELECT LPAD(COUNT(pk_usuario)+1,3,0) FROM sig_usuario WHERE pk_usuario LIKE '"+val+"%'"));
        val = val+correlativo;
        return (val.length() == 1 ? val : val);
    }


    @Override
    public SigCentro buscarCentro(int id) {
        return (SigCentro) currentSession().createQuery("From SigCentro where pkIdCentro = :id").setParameter("id", id).uniqueResult();
    }

    @Override
    public List<SigUbicacion> listPorDepto(String departamentoSeleccionado) {
                return currentSession().createCriteria(SigUbicacion.class)
                .add(Restrictions.eq("NOM_DEPTO",departamentoSeleccionado))
                .list();
    }

    @Override
    public String departamentoSeleccionado(String departamentoSeleccionado) {
    return  currentSession().createCriteria(SigUbicacion.class)
                .add(Restrictions.eq("NOM_DEPTO",departamentoSeleccionado))
                .toString();
    
    }

}

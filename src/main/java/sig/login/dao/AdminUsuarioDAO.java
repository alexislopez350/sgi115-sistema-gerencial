/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.dao;

import sig.login.entity.SigCentro;
import java.util.List;
import sig.login.entity.SigMenu;
import sig.login.entity.SigPermiso;
import sig.login.entity.SigRol;
import sig.login.entity.SigUbicacion;
import sig.login.entity.SigUsuario;

/**
 *
 * @author Administrador
 */
public interface AdminUsuarioDAO {
    //ROL
    List<SigRol> listRol();
    List<SigRol> listRolTodos();
    
    //MENU
    List<SigPermiso> listMenu(int rol);
    List<SigMenu> listMenuAll();
    
    //USUARIO
    List<SigUsuario> listUsuario();
    List<SigUsuario> listaPorCentro(int centro);
    String consultarCodigo(String nomApell);
    
    //CENTRO
    List<SigCentro> listCentro();
    List<SigCentro> listCentro2(int centro);
    List<SigCentro> listCentroTodos();

    public List<SigUbicacion> listPorDepto(String departamentoSeleccionado);

    public String departamentoSeleccionado(String departamentoSeleccionado);

    public SigCentro buscarCentro(int id);

}

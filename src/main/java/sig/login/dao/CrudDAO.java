/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.dao;

/**
 *
 * @author Administrador
 */
public interface CrudDAO {
    void insertObj(Object obj);
    void insertUpdateObj(Object obj);
    void updateObj(Object obj);
    void deleteObj(Object obj);
    String maxTblSql(String sql);
    String maxRubro(String sql);
    String idMenu(String sql);
    String consultarParametro(int cod);
    void scriptSQL(String sql);
    String scriptSQLKardex(String sql);
}

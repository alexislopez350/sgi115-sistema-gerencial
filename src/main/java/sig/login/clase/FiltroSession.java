/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.clase;

import java.io.IOException;
import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Admin
 */
@WebFilter("/secure/*")
@Getter
@Setter
public class FiltroSession implements Filter {

    FilterConfig filterConfig;

    private static final String AJAX_REDIRECT_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<partial-response><redirect url=\"%s\"></redirect></partial-response>";

    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        try {

            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse resp = (HttpServletResponse) response;
            HttpSession session = req.getSession(true);

            /*
                NO CACHE, NO BACK BUTTON
             */
            if (!req.getRequestURI().startsWith(req.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER)) { // Skip JSF resources (CSS/JS/Images/etc)
                resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
                resp.setHeader("Pragma", "no-cache"); // HTTP 1.0.
                resp.setDateHeader("Expires", 0); // Proxies.
            }

            /*
                INCLUIR PETICIONES AJAX
             */
            boolean ajaxRequest = "partial/ajax".equals(req.getHeader("Faces-Request"));
            String user = (session != null) ? (String) session.getAttribute("codigoLogeado") : null;
            if (user != null) {
//                if (req.getRequestURI().contains(user.getFuncion().getDirUrl().substring(0, 9))) {
                    chain.doFilter(request, resp);
//                } else {
//                    resp.sendRedirect(user.getFuncion().getDirUrl());
//                }
            } else if (ajaxRequest) {
                resp.setContentType("text/xml");
                resp.setCharacterEncoding("UTF-8");
                resp.getOutputStream().print(String.format(AJAX_REDIRECT_XML, (req.getContextPath() + "/login.sig")));
            } 
            else {
                resp.sendRedirect((req.getContextPath() + "/login.sig"));
            }
        } catch (Exception e) {
            System.out.println("***** ERROR FiltroSession **** "+e);
        }
    }

    @Override
    public void destroy() {
        this.filterConfig = null;
    }

}

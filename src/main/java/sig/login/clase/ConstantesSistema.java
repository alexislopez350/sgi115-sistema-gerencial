/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.clase;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Alexis
 */
public class ConstantesSistema {
    public final static String RP_NOMBRE_SISTEMA = "SIG";
    public final static String RP_RUTA_REPORTE_JASPER = 
            "/"+RP_NOMBRE_SISTEMA+"/Reportes/Reportes/";
    public final static String RP_EXTESION_REPORTE = ".jasper";
    public final static String EXTESION_PAG = ".sig115";
    public final static String RUTA_ARCH_VARIOS_SUBIDOS_TEMP = 
            "/"+RP_NOMBRE_SISTEMA+"/ArchivosSubidoTemp/";
    private static FacesMessage message;
    
    /*RUTA DE OTROS SISTEMAS*/
    
    
    public static void ERROR_SISTEMA(String lugar,Object e){
        System.out.println("**** ERROR *** "+RP_NOMBRE_SISTEMA+" *** "+lugar+"  *** ERROR:"+e);
        message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error del Sistema", "");
        FacesContext.getCurrentInstance().addMessage(null, message);
        message = null;
    }
}

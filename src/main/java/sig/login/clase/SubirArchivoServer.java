/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.clase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import static sig.login.clase.ConstantesSistema.ERROR_SISTEMA;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author AAldemaro
 */
public class SubirArchivoServer {

    /*
     carpetaDireccionDocs : RUTA Y NOMBRE LA CARPETA 
     renombreArchivo      : NOMBRE DEL ARCHIVO RENOMBRADO
     */
    private InputStream inputStream;
    private OutputStream outputStream;
    private File directorio;
    private Integer read;
    private byte[] bytes;

    public String SubirArchivo(FileUploadEvent event, String carpetaDireccionDocs, String renombreArchivo) {

        inputStream = null;
        outputStream = null;

        try {
            directorio = new File(carpetaDireccionDocs);
            if (!directorio.exists()) {
                directorio.mkdirs();
            }

            outputStream = new FileOutputStream(new File(carpetaDireccionDocs
                    + renombreArchivo + event.getFile().getFileName().substring(event.getFile().getFileName().lastIndexOf("."))));
            inputStream = event.getFile().getInputstream();
            read = 0;
            bytes = new byte[(int) event.getFile().getSize()];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            return carpetaDireccionDocs
                    + renombreArchivo + event.getFile().getFileName().substring(event.getFile().getFileName().lastIndexOf("."));

        } catch (IOException e) {
            ERROR_SISTEMA("SubirArchivo.SubirArchivo 1 ", e);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                ERROR_SISTEMA("SubirArchivo.SubirArchivo 2", e);
            }
            inputStream = null;
            outputStream = null;
            directorio = null;
            read = null;
            bytes = null;
        }
        return "";
    }

    // urlDoc URL Y NOMBRE DEL DOCUMENTO
    public StreamedContent descargarArchivo(String urlDoc) {
        try {
            directorio = new File(urlDoc);
            inputStream = new FileInputStream(directorio);
            return (new DefaultStreamedContent(inputStream,
                    FacesContext.getCurrentInstance().getExternalContext().getMimeType(urlDoc),
                    directorio.getName()));
        } catch (FileNotFoundException e) {
            ERROR_SISTEMA("SubirArchivo.descargarArchivo", e);
        } finally {
            directorio = null;
            inputStream = null;
        }
        return null;
    }

    public void borrarArchivo(String urlNombreDoc) {
        try {
            directorio = new File(urlNombreDoc);
            if (directorio.exists()) {
                directorio.delete();
            }
            directorio = null;
        } catch (Exception e) {
            ERROR_SISTEMA("SubirArchivo.borrarArchivo", e);
        }
    }
}

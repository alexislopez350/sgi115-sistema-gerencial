/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bean;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.context.RequestContext;
import sig.login.bo.AdminUsuarioBO;
import sig.login.clase.Mensajes;
import sig.login.entity.SigRol;

/**
 *
 * @author Administrador
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class RolBean extends Mensajes {

    @ManagedProperty(value = "#{adminUsuarioBO}")
    private AdminUsuarioBO adminUsuarioBO;
    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;

    private SigRol rol;
    private List<SigRol> listRol;

    public RolBean() {
        rol = new SigRol();
    }

    @PostConstruct
    public void main() {
        listRol = adminUsuarioBO.listRolTodos();
    }

    public void insertRol() {
        if (adminUsuarioBO.insertRol(this, sessionbean)) {
            mensaje(1, "Rol agregado correctamente");
            rol = new SigRol();
            listRol = adminUsuarioBO.listRolTodos();
        } else {
            mensaje(2, "Error al agregar el Rol");
        }
    }

    public void updateEstadoRol(SigRol ro) {
        ro.setEstado(!ro.isEstado());
        if (adminUsuarioBO.updateRol(ro, sessionbean)) {
            mensaje(1, "Estado del rol modificado");
            listRol = adminUsuarioBO.listRolTodos();
        } else {
            mensaje(2, "Error al modificar el estado del rol");
        }
    }

    public void onRowEdit(SigRol ro) {
        if (adminUsuarioBO.updateRol(ro, sessionbean)) {
            mensaje(1, "Rol modificado correctamente");
        } else {
            mensaje(2, "Error al modificar el rol");
        }
    }

    public void onRowCancel() {
        mensaje(2, "Modificación cancelada");
    }

    public void rolSeleccionado(SigRol ro) {
        if (listRol.size() > 0) {
            for(SigRol rd:listRol){
                rd.setRolS(false);
                if(rd.getPkRol() == ro.getPkRol()){
                    rd.setRolS(true);
                }
            }
        }
    }
}

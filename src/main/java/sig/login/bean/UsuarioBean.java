/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bean;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import lombok.Getter;
import lombok.Setter;
import sig.login.bo.AdminUsuarioBO;
import sig.login.clase.Email;
import sig.login.clase.Mensajes;
import sig.login.entity.SigCentro;
import sig.login.entity.SigRol;
import sig.login.entity.SigUsuario;

/**
 *
 * @author Administrador
 */
@Getter
@Setter
@ManagedBean
@ViewScoped
public class UsuarioBean extends Mensajes {

    @ManagedProperty(value = "#{adminUsuarioBO}")
    private AdminUsuarioBO adminUsuarioBO;
    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;
    //private BitacoraBean bitbean;
    private List<SigUsuario> listUsu;
    private SigUsuario usu;
    private List<SigRol> listRol;
    private List<SigCentro> listCentro;
    //private int idCentroSeleccionado;
    //private List<SigUsuario> listPorCentro;

    public UsuarioBean() {
    }

    @PostConstruct
    public void main() {
        usu = new SigUsuario();
        listUsu = adminUsuarioBO.listUsuario();
        listRol = adminUsuarioBO.listRol();
        listCentro=adminUsuarioBO.listCentro();
        //listPorCentro=adminUsuarioBO.listaPorCentro(centro);
    }

    public void generarCodigo() {
        usu.setCodigo(adminUsuarioBO.generarCodigo(usu.getNombre().substring(0, 1) + usu.getApellido().substring(0, 1)));
    }

    public void insertUsuario() {
        if (usu.getCodigo() != null) {
            /*if (*/adminUsuarioBO.insertUsuario(this, sessionbean)/*) {*/;
                /*mensaje(1, "Usuario agregado correctamente");
                try {
                    Email emai = new Email();
                    emai.setListDestinatarios(new ArrayList<InternetAddress>());
                    emai.getListDestinatarios().add(new InternetAddress(usu.getCorreo()));
                    emai.setAsunto("NUEVO-USUARIO");
                    emai.setMensaje("Por este medio se le comunica que ha creado una cuenta para que pueda acceder al Sistema Informatico"
                            + "\n de Control de Alimentacion en Centros ISNA y Resguardo '('SICACIR:')"
                            + "\nSu Usuario: " + usu.getCodigo() + "\n"
                            + "Contraseña: nuevo"
                            + "\nNOTA:"
                            + "\nSu contraseña por defecto es 'nuevo' a la hora de ingresar por primera vez en el sistema le pedira cambiar "
                            + "\n la contraseña, por favor cambiarla por una que solo usted la sepa");
                    if (emai.sendMail()) {
                        mensaje(1, "Correo Enviado de Forma Exitosa");
                    } else {
                        mensaje(2, "No fue Posible enviar el correo electrico, problemente esten bloqueados los puertos!");
                    }
                } catch (AddressException e) {
                    mensaje(2, "No se pudo mandar el correo por falta de internet");
                    System.out.println("*** ERROR EN ENVIO DE CORREO *** " + e);
                }*/
                listUsu = adminUsuarioBO.listUsuario();
                usu = new SigUsuario();/*
            } else {
                mensaje(2, "Error al Agregar Nuevo Usuario");
            }*/
        }else{
            mensaje(2, "Error no se ha generado el Codigo del Usuario");
        }
    }

    public void updateEstadoUsu(SigUsuario us) {
        us.setEstado(!us.isEstado());
        if (adminUsuarioBO.updateUsuario(us, sessionbean)) {
            listUsu = adminUsuarioBO.listUsuario();
            mensaje(1, "Estado del usuario correctamente");
        } else {
            mensaje(2, "Error al modificar el estado");
        }
    }

    public void resetClave(SigUsuario us) {
        us.setClave("e26c062fedf6b32834e4de93f9c8b644");
        if (adminUsuarioBO.updateUsuario(us, sessionbean)) {
            listUsu = adminUsuarioBO.listUsuario();
            mensaje(1, "clave reiniciada correctamente");
        } else {
            mensaje(2, "Error al reiniciar la clave");
        }
    }

    public void onRowEdit(SigUsuario us) {
        if (adminUsuarioBO.updateUsuario(us, sessionbean)) {
            mensaje(1, "Usuario modificado correctamente");
        } else {
            mensaje(2, "Error al modificar el usuario");
        }
    }

    public void onRowCancel() {
        mensaje(2, "Modificación cancelada");
    }
   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.context.RequestContext;
import sig.login.bo.AdminUsuarioBO;
import sig.login.clase.Mensajes;
import sig.login.entity.SigIcono;
import sig.login.entity.SigMenu;
import sig.login.entity.SigPermiso;
import sig.login.entity.SigRol;

/**
 *
 * @author Administrador
 */
@Getter
@Setter
@ManagedBean
@ViewScoped
public class MenuBean extends Mensajes {

    @ManagedProperty(value = "#{adminUsuarioBO}")
    private AdminUsuarioBO adminUsuarioBO;
    @ManagedProperty(value = "#{busIconoBean}")
    private BusIconoBean busIconoBean;
    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;

    private SigRol rolSelec;
    private List<SigPermiso> listMenu;
    private SigMenu menu;

    //DIALOGO
    private List<SigMenu> listMenuAll;

    private boolean estadoMosMen;

    public MenuBean() {
        menu = new SigMenu();
        menu.setSigIcono(new SigIcono());
        estadoMosMen = false;
    }

    public void rolSelect(SigRol rol) {
        rolSelec = rol;
        estadoMosMen = true;
        listMenu = adminUsuarioBO.listMenu(rolSelec.getPkRol());
    }

    public void mostrarDiag() {
        listMenuAll = adminUsuarioBO.listMenuAll();
        menu = new SigMenu();
        RequestContext.getCurrentInstance().execute("PF('menuWid').show();");
        RequestContext.getCurrentInstance().update("formMenu");
    }

    public void insertNewMenu() {
        if (adminUsuarioBO.insertMenu(this)) {
            menu = new SigMenu();
            listMenuAll = adminUsuarioBO.listMenuAll();
//            listMenu = adminUsuarioBO.listMenu(rolSelec.getPkRol());
//            RequestContext.getCurrentInstance().execute("PF('menuWid').hide();");
            mensaje(1, "Menu agregado correctamente");
        } else {
            mensaje(2, "Error al agregar el menu");
        }
    }

    public void deleteMenu(SigPermiso per) {
        if (adminUsuarioBO.deleteMenuPer(per)) {
            listMenu = adminUsuarioBO.listMenu(rolSelec.getPkRol());
            mensaje(1, "Menu eliminado correctamente");
        } else {
            mensaje(2, "Error al eliminar el menu");
        }
    }

//    public void onRowEdit(DsiMenu me) {
    public void onRowEdit(SigPermiso pe) {
        if (adminUsuarioBO.updatePermiso(pe)) {
            listMenu = adminUsuarioBO.listMenu(rolSelec.getPkRol());
            mensaje(1, "Orden modificado correctamente");
            RequestContext.getCurrentInstance().execute("document.getElementById('formRol:btnResFor').click();");
        } else {
            mensaje(2, "Error al modificar el orden");
        }
    }

    public void onRowCancel() {
        mensaje(2, "Modificación cancelada");
    }

    public void insertMenuRol(SigMenu me) {
        if (adminUsuarioBO.insertMenuRol(this, me)) {
            listMenu = adminUsuarioBO.listMenu(rolSelec.getPkRol());
            menu = new SigMenu();
            listMenuAll = new ArrayList<>();
            RequestContext.getCurrentInstance().update("formRol");
            RequestContext.getCurrentInstance().update("formMenu");
            RequestContext.getCurrentInstance().execute("PF('menuWid').hide();");
            mensaje(1, "Menu agregado correctamente");
        } else {
            mensaje(2, "Error al agregar el menu");
        }
    }

    public void mostrarBusIcono() {
        busIconoBean.limpiar();
        busIconoBean.setBean("menuBean");
        busIconoBean.setMetodo("iconoSelecc");
        busIconoBean.mostrarBusc();
    }

    public void iconoSelecc(SigIcono ico) {
        menu.setSigIcono(ico);
        RequestContext.getCurrentInstance().update("formMenu");
    }
}

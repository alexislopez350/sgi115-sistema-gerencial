/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bo;

import java.util.List;
import org.primefaces.model.TreeNode;
import sig.login.entity.SigIcono;

/**
 *
 * @author User
 */
public interface BuscadorBO {
    //ICONO
    List<SigIcono> listIcono();
    boolean insertIcono(SigIcono ico);
    boolean updateIcono(SigIcono ico);
    String consultarParametro(int cod);
}

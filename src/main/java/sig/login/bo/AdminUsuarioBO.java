/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bo;

import sig.login.bean.CentroBean;
import java.util.List;
import sig.login.bean.MenuBean;
import sig.login.bean.RolBean;
import sig.login.bean.SessionBean;
import sig.login.bean.UsuarioBean;
import sig.login.entity.SigCentro;
import sig.login.entity.SigMenu;
import sig.login.entity.SigPermiso;
import sig.login.entity.SigRol;
import sig.login.entity.SigUbicacion;
import sig.login.entity.SigUsuario;

/**
 *
 * @author Administrador
 */
public interface AdminUsuarioBO {
    //ROL
    List<SigRol> listRol();
    List<SigRol> listRolTodos();
    boolean insertRol(RolBean bean, SessionBean usu);
    boolean updateRol(SigRol rol, SessionBean usu);
     //CENTRO
    List<SigCentro> listCentro();
    List<SigCentro> listCentro2(int centro);
    List<SigCentro> listCentroTodos();
    boolean insertCentro(CentroBean bean, SessionBean usu);
    boolean updateCentro(SigCentro SigCentro, SessionBean usu);
    
    //MENU
    List<SigPermiso> listMenu(int rol);
    List<SigMenu> listMenuAll();
    boolean insertMenu(MenuBean bean);
    boolean deleteMenu(SigMenu me);
    boolean deleteMenuPer(SigPermiso per);
    boolean updateMenu(SigMenu me);
    boolean updatePermiso(SigPermiso pe);
    boolean insertMenuRol(MenuBean bean,SigMenu me);
    
    //USUARIO
    List<SigUsuario> listUsuario();
    List<SigUsuario> listaPorCentro(int centro);
    String generarCodigo(String nomApell);
    boolean insertUsuario(UsuarioBean bean, SessionBean usu);
    boolean updateUsuario(SigUsuario usuario, SessionBean usu);  

    public List<SigUbicacion> listPorDepto(String departamentoSeleccionado);
    public SigCentro buscarCentro(int id);
    public String obtenerDepto(String departamentoSeleccionado);
}

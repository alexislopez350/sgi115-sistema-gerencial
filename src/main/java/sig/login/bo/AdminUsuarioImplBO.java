/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bo;

import sig.login.bean.CentroBean;
import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sig.login.bean.MenuBean;
import sig.login.bean.RolBean;
import sig.login.bean.SessionBean;
import sig.login.bean.UsuarioBean;
import sig.login.dao.AdminUsuarioDAO;
import sig.login.dao.CrudDAO;
import sig.login.entity.SigCentro;
import sig.login.entity.SigMenu;
import sig.login.entity.SigPermiso;
import sig.login.entity.SigPermisoId;
import sig.login.entity.SigRol;
import sig.login.entity.SigUbicacion;
import sig.login.entity.SigUsuario;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Administrador
 */
@Getter
@Service(value = "adminUsuarioBO")
public class AdminUsuarioImplBO implements AdminUsuarioBO {

    @Autowired
    private CrudDAO crudDAO;
    @Autowired
    private AdminUsuarioDAO adminUsuarioDAO;

    @Override
    public List<SigRol> listRol() {
        return adminUsuarioDAO.listRol();
    }

    @Override
    public List<SigRol> listRolTodos() {
        return adminUsuarioDAO.listRolTodos();
    }

    @Override
    public List<SigCentro> listCentroTodos() {
        return adminUsuarioDAO.listCentroTodos();
    }

    @Override
    public boolean insertRol(RolBean bean, SessionBean usu) {
        try {
            bean.getRol().setPkRol(Integer.valueOf(crudDAO.maxTblSql("SELECT coalesce(MAX(pk_rol),0)+1 FROM sig_rol")));
            bean.getRol().setEstado(true);
            crudDAO.insertObj(bean.getRol());
            crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'" + usu.getUsu().getCodigo() + "', '" + usu.getUsu().getSigRol().getPkRol() + "', '" + usu.getUsu().getSigCentro().getPkIdCentro() + "','sig_rol','INSERT', '" + bean.getRol().toString() + "')");
            return true;
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** Insert Rol ** " + e);
            return false;
        }
    }

    @Override()
    public boolean insertCentro(CentroBean bean, SessionBean usu) {
        try {
            bean.getSigCentro().setPkIdCentro(Integer.valueOf(crudDAO.maxTblSql("SELECT coalesce(MAX(pk_idCentro),0)+1 FROM sig_centro")));

            crudDAO.insertObj(bean.getSigCentro());
            crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'" + usu.getUsu().getCodigo() + "', '" + usu.getUsu().getSigRol().getPkRol() + "', '" + usu.getUsu().getSigCentro().getPkIdCentro() + "','sig_centro','INSERT', '" + bean.getSigCentro().toString() + "')");
            return true;
        } catch (Exception e) {
            System.out.println("*** AdminUsuarioImpBo ** Insert Centro ** " + e);
            return false;
        }
    }

    @Override
    public boolean updateCentro(SigCentro sigCentro, SessionBean usu) {
        try {
            if ((sigCentro.getTelefono() != "") && (sigCentro.getTelefono().length() == 9)) {
                crudDAO.updateObj(sigCentro);
                crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'" + usu.getUsu().getCodigo() + "', '" + usu.getUsu().getSigRol().getPkRol() + "', '" + usu.getUsu().getSigCentro().getPkIdCentro() + "','sig_centro','UPDATE', '" + sigCentro.toString() + "')");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Centro Modificado Correctamente!"));
                return true;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("No es posible Modificar el Centro, el numero de telefono no es valido!"));
                return false;
            }
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** Update Centro ** " + e);
            return false;
        }
    }

    @Override
    public boolean updateRol(SigRol rol, SessionBean usu) {
        try {
            crudDAO.updateObj(rol);
            crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'" + usu.getUsu().getCodigo() + "', '" + usu.getUsu().getSigRol().getPkRol() + "', '" + usu.getUsu().getSigCentro().getPkIdCentro() + "','sig_rol','UPDATE', '" + rol.toString() + "')");
            return true;
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** Update Rol ** " + e);
            return false;
        }
    }

    @Override
    public boolean insertMenu(MenuBean bean) {
        try {
            bean.getMenu().setCodigo(
                    Integer.valueOf(
                            crudDAO.maxTblSql("SELECT coalesce(MAX(pk_menu),0)+1 FROM sig_menu")
                    )
            );
            crudDAO.insertObj(bean.getMenu());
            return true;
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** insert menu ** " + e);
            return false;
        }
    }

    @Override
    public List<SigPermiso> listMenu(int rol) {
        return adminUsuarioDAO.listMenu(rol);
    }

    @Override
    public List<SigMenu> listMenuAll() {
        return adminUsuarioDAO.listMenuAll();
    }

    @Override
    public boolean deleteMenu(SigMenu me) {
        try {
            crudDAO.deleteObj(me);
            return true;
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** delete menu ** " + e);
            return false;
        }
    }

    @Override
    public boolean deleteMenuPer(SigPermiso per) {
        try {
            crudDAO.deleteObj(per);
            return true;
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** deletePermiso menu ** " + e);
            return false;
        }
    }

    @Override
    public boolean updateMenu(SigMenu me) {
        try {
            crudDAO.updateObj(me);
            return true;
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** update menu ** " + e);
            return false;
        }
    }

    @Override
    public boolean updatePermiso(SigPermiso pe) {
        try {
            crudDAO.updateObj(pe);
            return true;
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** update permiso ** " + e);
            return false;
        }
    }

    @Override
    public boolean insertMenuRol(MenuBean bean, SigMenu me) {
        try {
            SigPermiso per = new SigPermiso();
            per.setId(new SigPermisoId(bean.getRolSelec().getPkRol(), me.getCodigo()));
            per.setOrden(Short.valueOf(
                    crudDAO.maxTblSql("SELECT coalesce(MAX(orden),0)+1 "
                            + "FROM sig_permiso "
                            + "WHERE fk_rol = '" + bean.getRolSelec().getPkRol() + "' ")
            ));
            crudDAO.insertObj(per);
            return true;
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** insert menu rol ** " + e);
            return false;
        }
    }

    @Override
    public List<SigUsuario> listUsuario() {
        return adminUsuarioDAO.listUsuario();
    }

    @Override
    public List<SigUsuario> listaPorCentro(int centro) {
        return adminUsuarioDAO.listaPorCentro(centro);
    }

    @Override
    public String generarCodigo(String nomApell) {
        return adminUsuarioDAO.consultarCodigo(nomApell);
    }

    @Override
    public boolean insertUsuario(UsuarioBean bean, SessionBean usu) {
        try {
            bean.getUsu().setEstado(true);
            bean.getUsu().setClave("e26c062fedf6b32834e4de93f9c8b644");
            crudDAO.insertObj(bean.getUsu());
            crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'" + usu.getUsu().getCodigo() + "', '" + usu.getUsu().getSigRol().getPkRol() + "', '" + usu.getUsu().getSigCentro().getPkIdCentro() + "','sig_usuario','INSERT', '" + bean.getUsu().toString() + "')");
            return true;
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** insert usuario ** " + e);
            return false;
        }
    }

    @Override
    public boolean updateUsuario(SigUsuario usuario, SessionBean usu) {
        try {
            crudDAO.updateObj(usuario);
            crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'" + usu.getUsu().getCodigo() + "', '" + usu.getUsu().getSigRol().getPkRol() + "', '" + usu.getUsu().getSigCentro().getPkIdCentro() + "','sig_musuario','UPDATE', '" + usuario.toString() + "')");
            return true;
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** insert usuario ** " + e);
            return false;
        }
    }

    @Override
    public List<SigCentro> listCentro() {
        return adminUsuarioDAO.listCentro();
    }

    @Override
    public List<SigCentro> listCentro2(int centro) {
        return adminUsuarioDAO.listCentro2(centro);
    }

    @Override
    public List<SigUbicacion> listPorDepto(String departamentoSeleccionado) {
        return adminUsuarioDAO.listPorDepto(departamentoSeleccionado);
    }

    @Override
    public String obtenerDepto(String departamentoSeleccionado) {
        return adminUsuarioDAO.departamentoSeleccionado(departamentoSeleccionado);
    }

    @Override
    public SigCentro buscarCentro(int id) {
        return adminUsuarioDAO.buscarCentro(id);
    }
}

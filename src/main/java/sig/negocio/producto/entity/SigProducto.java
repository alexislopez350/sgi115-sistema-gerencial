package sig.negocio.producto.entity;
// Generated 09-22-2019 02:26:06 PM by Hibernate Tools 4.3.1

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Alexis Lopez
 */
@Entity
@Table(name = "sig_producto",
         catalog = "sig115"
)
@Getter
@Setter
@NoArgsConstructor
public class SigProducto implements Serializable {

    public SigProducto(String codigo) {
        this.codigo = codigo;
    }
 
    @Id
    @Column(name = "pk_producto", unique = true, nullable = false, length = 19)
    private String codigo;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_rubro", nullable = false)
    private SigRubro sigRubro;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_TipoProducto", nullable = false)
    private SigTipoproducto sigTipoproducto;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_UnidadMedida", nullable = false)
    private SigUnidadmedida sigUnidadmedida;
    
    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;
    
    @Column(name = "descrip", nullable = false, length = 250)
    private String descrip;
  
    @Column(name = "presentacion", nullable = false, length = 50)
    private String presentacion;
    
    @Column(name = "estado", nullable = false)
    private boolean estado;

    @Transient
    private Integer rubroS;
    
    @Override
    public String toString(){
        String est;
        if(estado==true) est= "Activo";      
        else est= "Inactivo";
        return "Nombre= "+nombre+", Descripcion= "+descrip+", Presentacion= "+presentacion+", TipoProducto= "+sigTipoproducto.getNombre()+", UnidadMedida= "+sigUnidadmedida.getNombre()+", Estado= "+est+". ";
    }
}

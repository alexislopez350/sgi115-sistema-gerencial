/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.negocio.producto.bo;

import sig.dto.IngresoBodegaDto;
import sig.login.bean.SessionBean;
import java.util.List;
import sig.negocio.producto.entity.SigProducto;
import sig.negocio.producto.entity.SigTipoproducto;
import sig.negocio.producto.entity.SigUnidadmedida;
import sig.negocio.producto.entity.SigRubro;
import sig.negocio.producto.bean.ProductoBean;
import sig.negocio.producto.bean.RubroBean;

/**
 *
 * @author Alexis Lopez
 */

public interface AdminProductoBO {
    //TIPOPRODUCTO
    List<SigTipoproducto> listTipo();
    List<SigTipoproducto> listTipoTodos();
    
    //UNIDADMEDIDA
    List<SigUnidadmedida> listUnidad();
    List<SigUnidadmedida> listUnidadTodos();
    
    //PRODUCTO
    List<SigProducto> listProducto();
    List<SigProducto> listProdPere(int tipo);
    List<IngresoBodegaDto> listProCentroBodega(int centro);
    boolean insertProducto(ProductoBean bean, SessionBean usu);
    boolean updateProducto(SigProducto pro, SessionBean usu);

    //RUBRO
    public List<SigRubro> listRubro();
    public List<SigRubro> listRubroActivo();
    public boolean insertRubro(RubroBean rubro, SessionBean usu);
    boolean updateRubro(SigRubro rubro, SessionBean usu);
    SigProducto encontrarProducto(String codProducto);
}
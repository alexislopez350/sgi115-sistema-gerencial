/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
*/


package sig.negocio.producto.bean;

import sig.login.bean.SessionBean;
import sig.negocio.producto.bo.AdminProductoBO;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import sig.login.clase.Mensajes;
import sig.login.dao.CrudDAO;
import sig.negocio.producto.entity.SigProducto;
import sig.negocio.producto.entity.SigRubro;
import sig.negocio.producto.entity.SigTipoproducto;
import sig.negocio.producto.entity.SigUnidadmedida;

/**
 *
 * @author Administrador
 */

@Getter
@Setter
@ManagedBean
@ViewScoped
public class ProductoBean extends Mensajes {
    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;
    @ManagedProperty(value = "#{adminProductoBO}")
    private AdminProductoBO adminProductoBO;
    private List<SigProducto> proSelec;
    private List<SigProducto> listPro;
    private SigProducto pro;
    private List<SigTipoproducto> listTipo;
    private List<SigUnidadmedida> listUnidad;
    private List<SigRubro> listRubro;
    private List<SigRubro> listRubroActivo;
    private SigRubro rubroSelec;
    private boolean estadoMosMen;
    private CrudDAO crudDAO;

    public ProductoBean() {
        pro = new SigProducto();
    }

    @PostConstruct
    public void main() {
        pro = new SigProducto();
        listPro = adminProductoBO.listProducto();
        listTipo = adminProductoBO.listTipo();
        listUnidad = adminProductoBO.listUnidad();
        listRubro = adminProductoBO.listRubro();
        listRubroActivo = adminProductoBO.listRubroActivo();
    }

    public void insertProducto() {
            if (adminProductoBO.insertProducto(this,sessionbean)) {
                mensaje(1, "Producto agregado correctamente");
                pro = new SigProducto();
                listPro = adminProductoBO.listProducto();
            } else {
                mensaje(2, "Error al Agregar Nuevo Producto");
            }
    }
    
        /*public void rubroSelect(SigRubro rubro) {
        rubroSelec = rubro;
        estadoMosMen = true;
        listRubro = adminProductoBO.listRubro(rubroSelec.getIdRubro());
    }*/
    
    public void updateEstadoPro(SigProducto pr) {
        pr.setEstado(!pr.isEstado());
        if (adminProductoBO.updateProducto(pr,sessionbean)) {
            listPro = adminProductoBO.listProducto();
            mensaje(1, "Estado del producto modificado correctamente");
        } else {
            mensaje(2, "Error al modificar el estado");
        }
    }
    
    public void onRowEdit(SigProducto pr) {
        if (adminProductoBO.updateProducto(pr,sessionbean)) {
            mensaje(1, "Producto modificado correctamente");
        } else {
            mensaje(2, "Error al modificar el Producto");
        }
    }

    public void onRowCancel() {
        mensaje(2, "Modificación cancelada");
    }
    

}

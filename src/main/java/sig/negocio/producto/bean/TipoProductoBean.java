/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.negocio.producto.bean;

import sig.negocio.producto.entity.SigTipoproducto;
import sig.negocio.producto.bo.AdminProductoBO;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Alexis Lopez
 */
@Getter
@Setter
@ManagedBean
@ViewScoped
public class TipoProductoBean  {

    @ManagedProperty(value = "#{adminProductoBO}")
    private AdminProductoBO adminProductoBO;

    private List<SigTipoproducto> listTipo;
    private SigTipoproducto tipo;

    public TipoProductoBean() {
        tipo = new SigTipoproducto();
    }

    @PostConstruct
    public void main() {
        tipo = new SigTipoproducto();
        listTipo = adminProductoBO.listTipo();
    }
    
    public void tipoSeleccionado(SigTipoproducto ti) {
        if (listTipo.size() > 0) {
            for(SigTipoproducto td:listTipo){
                td.setTipoS(false);
                if(td.getCodigo() == ti.getCodigo()){
                    td.setTipoS(true);
                }
            }
        }
    }
}
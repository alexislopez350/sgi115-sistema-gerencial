/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.negocio.producto.dao;

import sig.dto.IngresoBodegaDto;
import sig.negocio.producto.entity.SigProducto;
import sig.negocio.producto.entity.SigRubro;
import sig.negocio.producto.entity.SigTipoproducto;
import sig.negocio.producto.entity.SigUnidadmedida;
import java.util.List;
import lombok.Getter;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Alexis Lopez
 */
@Getter
@Transactional(value = "TMA")
@Service(value = "adminProductoDAO")
public class AdminProductoImplDAO extends HibernateDaoSupport implements AdminProductoDAO {
    
    @Autowired
    public AdminProductoImplDAO(@Qualifier(value = "SFA") SessionFactory sf) {
        super.setSessionFactory(sf);
    }

    @Override
    public List<SigProducto> listProducto() {
        return currentSession().createCriteria(SigProducto.class)
                .addOrder(Order.asc("codigo"))
                .list();
    }
    
        @Override
    public List<IngresoBodegaDto> listProCentroBodega(int centro) {
        return currentSession().createQuery("select new sig.dto.IngresoBodegaDto(i.pk_ExistenciaBodega, p.codigo, p.nombre, p.descrip, sum(i.cantidad), p.presentacion) "
                + "from SigExistenciasBodega i inner join i.fk_codigo p "
                + "where i.fk_idCentro=:id "
                + "group by i.fk_codigo").setParameter("id", centro).list();
    }

    @Override
    public List<SigProducto> listProdPere(int tipo) {
        return currentSession().createQuery("from SigProducto where idTipoProducto=:idTipoProducto ").setParameter("idTipoProducto",tipo).list();
    }

    @Override
    public List<SigTipoproducto> listTipo() {
           return currentSession().createCriteria(SigTipoproducto.class)
                .addOrder(Order.asc("nombre"))
                .list();
    }

    @Override
    public List<SigTipoproducto> listTipoAll() {
                return currentSession().createCriteria(SigTipoproducto.class)
                .addOrder(Order.asc("nombre"))
                .list();
    }

    @Override
    public List<SigUnidadmedida> listUnidadTodos() {
        return currentSession().createCriteria(SigUnidadmedida.class)
                .addOrder(Order.asc("nombre"))
                .list();
    }

    @Override
    public List<SigUnidadmedida> listUnidad() {
        return currentSession().createCriteria(SigUnidadmedida.class)
                .addOrder(Order.asc("nombre"))
                .list();
    }
    
    @Override
    public List<SigRubro> listRubro() {
        return currentSession().createCriteria(SigRubro.class)
                .addOrder(Order.asc("descripcion"))
                .list();
    }
    
    @Override
    public List<SigRubro> listRubroActivo() {
        return currentSession().createCriteria(SigRubro.class)
                .addOrder(Order.asc("descripcion"))
                .add(Restrictions.eq("estado", true))
                .list();
    }

    @Override
    public SigProducto encontrarProducto(String codProducto) {
       return (SigProducto) currentSession().createQuery("from SigProducto where codigo=:codigo").setParameter("codigo",codProducto).uniqueResult();
    }
    
}

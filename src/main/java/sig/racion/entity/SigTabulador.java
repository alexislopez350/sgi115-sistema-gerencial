/* Utilizado Alexis/*

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.racion.entity;

import sig.login.entity.SigCentro;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Elsy
 */
@Entity
@Table(name = "sig_tabulador",catalog="sig115")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SigTabulador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PK_TABULADOR")
    private Integer idTabulador;
    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "TOTAL")
    private Integer total;
    @JoinColumn(name = "FK_BENEFICIARIO", referencedColumnName = "pk_beneficiario")
    @ManyToOne(fetch=FetchType.LAZY, cascade= {CascadeType.ALL})
    private SigBeneficiario sigBeneficiario;
    @JoinColumn(name = "FK_TCOMIDA",referencedColumnName = "PK_TCOMIDA")
    @ManyToOne(fetch=FetchType.LAZY, cascade= {CascadeType.ALL})
    private SigTiempocomida sigTcomida;
    @JoinColumn(name = "fk_centro", referencedColumnName = "pk_centro") 
    @ManyToOne(fetch=FetchType.LAZY)
    private SigCentro sigCentro;

    @Override
    public String toString() {
        return "Fecha= " + fecha + ", Total= " + total + ", ID Personal= [" + sigBeneficiario.toString() + "], ID Tiempo comida= [" + sigTcomida.toString() + "], Centro= " + sigCentro.getNomCentro() + '.';
    }

    
    
}

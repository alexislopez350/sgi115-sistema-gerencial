/* Utilizado Alexis/*

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.racion.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Elsy
 */
@Entity
@Table(name = "sig_beneficiario", catalog = "sig115")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SigBeneficiario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PK_BENEFICIARIO")
    private Integer pkBeneficiario;
    @Column(name = "PERSONAL")
    private Integer personal;
    @Column(name = "HOMBRES") 
    private Integer hombres;
    @Column(name = "MUJERES")
    private Integer mujeres;
    @Column(name = "PNC")
    private Integer pnc;
    @Column(name = "NIÑOS")
    private Integer niños;
    @Column(name = "NIÑAS")
    private Integer niñas;
    @Column(name = "ANCIANOS")
    private Integer ancianos;
    @Column(name = "ANCIANAS")
    private Integer ancianas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sigBeneficiario")
    private List<SigTabulador> tabuladorList;

    @Override
    public String toString() {
        return "ID Beneficiario= " + pkBeneficiario + ", Personal= " + personal + ", Hombres= " + hombres + ", Mujeres= " + mujeres + ", PNC= " + pnc + ", Niños= " + niños + ", Niñas= " + niñas + ", Ancianos= " + ancianos + ", Ancianas= " + ancianas  +  ", tabuladorList= " + tabuladorList + '.';
    }

}

/*Utilizado Alexis*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.racion.dto;

import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author Elsy
 */
public class SigTabuladorDTO {

    private Date fecha;
    private Integer personal;
    private Integer hombres;
    private Integer mujeres;
    private Integer pnc;
    private Integer niños;
    private Integer niñas;
    private Integer ancianos;
    private Integer ancianas;
    private BigInteger total;
    private Boolean desayuno;
    private Boolean almuerzo;
    private Boolean cena;
    private Boolean refrigerio;

    public SigTabuladorDTO() {
    }

    public SigTabuladorDTO(Date fecha, Integer personal, Integer hombres, Integer mujeres, Integer pnc, Integer niños, Integer niñas, Integer ancianos, Integer ancianas, BigInteger total, Boolean desayuno, Boolean almuerzo, Boolean cena, Boolean refrigerio) {
        this.fecha = fecha;
        this.personal = personal;
        this.hombres = hombres;
        this.mujeres = mujeres;
        this.pnc = pnc;
        this.niños = niños;
        this.niñas = niñas;
        this.ancianos = ancianos;
        this.ancianas = ancianas;
        this.total = total;
        this.desayuno = desayuno;
        this.almuerzo = almuerzo;
        this.cena = cena;
        this.refrigerio = refrigerio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getPersonal() {
        return personal;
    }

    public void setPersonal(Integer personal) {
        this.personal = personal;
    }

    public Integer getHombres() {
        return hombres;
    }

    public void setHombres(Integer hombres) {
        this.hombres = hombres;
    }

    public Integer getMujeres() {
        return mujeres;
    }

    public void setMujeres(Integer mujeres) {
        this.mujeres = mujeres;
    }

    public Integer getPNC() {
        return pnc;
    }

    public void setPNC(Integer pnc) {
        this.pnc = pnc;
    }

    public Integer getNiños() {
        return niños;
    }

    public void setNiños(Integer niños) {
        this.niños = niños;
    }

    public Integer getNiñas() {
        return niñas;
    }

    public void setNiñas(Integer niñas) {
        this.niñas = niñas;
    }

    public Integer getAncianos() {
        return ancianos;
    }

    public void setAncianos(Integer ancianos) {
        this.ancianos = ancianos;
    }

    public Integer getAncianas() {
        return ancianas;
    }

    public void setAncianas(Integer ancianas) {
        this.ancianas = ancianas;
    }

    public Boolean getDesayuno() {
        return desayuno;
    }

    public void setDesayuno(Boolean desayuno) {
        this.desayuno = desayuno;
    }

    public Boolean getAlmuerzo() {
        return almuerzo;
    }

    public void setAlmuerzo(Boolean almuerzo) {
        this.almuerzo = almuerzo;
    }

    public Boolean getCena() {
        return cena;
    }

    public void setCena(Boolean cena) {
        this.cena = cena;
    }

    public Boolean getRefrigerio() {
        return refrigerio;
    }

    public void setRefrigerio(Boolean refrigerio) {
        this.refrigerio = refrigerio;
    }

    public BigInteger getTotal() {
        return total;
    }

}

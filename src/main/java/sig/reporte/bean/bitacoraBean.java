/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.reporte.bean;

import sig.login.bo.AdminUsuarioBO;
import sig.login.entity.SigCentro;
import sig.login.entity.SigRol;
import sig.login.entity.SigUsuario;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Alexis Lopez
 */

@Getter
@Setter
@ManagedBean
@ViewScoped
public class bitacoraBean {

    @ManagedProperty(value = "#{adminUsuarioBO}")
    private AdminUsuarioBO adminUsuarioBO;

    private List<SigUsuario> listUsu;
    private String usuario;
    private List<SigCentro> listCentro;
    private List<SigRol> listRol;
    private int idCentro = 0;
    private String rol;
    private SigUsuario usu;
    private List<SigUsuario> usuariosPorCentro;

    public bitacoraBean() {
    }

    @PostConstruct
    public void main() {
        usu = new SigUsuario();
        listUsu = adminUsuarioBO.listUsuario();
        listRol = adminUsuarioBO.listRol();
        listCentro = adminUsuarioBO.listCentro();
    }

    public void verReporte() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try {

            //Instancia hacia la clase reporteClientes        
            ReporteBitacora rBitacora = new ReporteBitacora();

            FacesContext facesContext = FacesContext.getCurrentInstance();
            ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
            String ruta = servletContext.getRealPath("/reportes/ReporteBit.jasper");

            rBitacora.getReporte(ruta, usuario, idCentro, rol);
            FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void obtenerUsuarios() {
        if (idCentro == 0) {
            usuariosPorCentro = adminUsuarioBO.listUsuario();
        } else {
            usuariosPorCentro = adminUsuarioBO.listaPorCentro(this.idCentro);
        }
    }
}

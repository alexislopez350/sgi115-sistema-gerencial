package sig.reporte.bean;

import sig.dto.IngresoBodegaDto;
import sig.login.bo.AdminUsuarioBO;
import sig.login.entity.SigCatalogoMarca;
import sig.login.entity.SigCentro;
import sig.login.entity.SigUsuario;
import sig.negocio.producto.bo.AdminProductoBO;
import sig.bodega.bo.ExistenciaBodegaBO;
import sig.login.bean.SessionBean;
import sig.negocio.producto.entity.SigProducto;
import sig.negocio.producto.entity.SigRubro;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.Setter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Frank
 */
@Getter
@Setter
@ManagedBean
@ViewScoped
public class kardexBodegaBean {

    @ManagedProperty(value = "#{adminUsuarioBO}")
    private AdminUsuarioBO adminUsuarioBO;

    @ManagedProperty(value = "#{adminProductoBO}")
    private AdminProductoBO adminProductoBO;

    @ManagedProperty(value = "#{existenciaBodegaBO}")
    private ExistenciaBodegaBO existenciaBodegaBO;

    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;

    private SigUsuario usu;
    private SigProducto pro;
    private SigRubro rubro;
    private List<SigCentro> listCentro;
    private List<SigCentro> listCentro2;
    private List<SigRubro> listRubro;
    private List<IngresoBodegaDto> listProducto;
    private List<SigCatalogoMarca> listMarca;
    private String marca;
    private String producto;
    private int idcentro;
    private int idcentro2;
    private Date inicio;
    private Date fin;

    public kardexBodegaBean() {
    }

    @PostConstruct
    public void main() {
        usu = new SigUsuario();
        pro = new SigProducto();
        listCentro = adminUsuarioBO.listCentro();
        listCentro2 = adminUsuarioBO.listCentro2(sessionbean.getUsu().getSigCentro().getPkIdCentro());
        listRubro = adminProductoBO.listRubro();
        //producto = adminProductoBO.listProCentroBodega(this.idcentro);
    }

    public void obtenerProductos() {
        listProducto = adminProductoBO.listProCentroBodega(idcentro);
    }

    public void obtenerMarca() {
        listMarca = existenciaBodegaBO.listMarcaProBodega(producto);
    }

    public void inicioSeleccionado() {
        setFin(inicio);
        System.out.println("Fecha inicial:" + inicio);
    }

    public void finSeleccionado() {
        setInicio(fin);
        System.out.println("Fecha fin:" + fin);
    }

    public void verReporte(int idcentro, String producto, String marca, Date inicio, Date fin) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String ruta = servletContext.getRealPath("/reportes/KardexBodega.jasper");
        getReporteBodega(ruta, idcentro, producto, marca, inicio, fin);
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void getReporteBodega(String ruta, int idCentro, String Producto, String Marca, Date Inicio, Date Fin) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Connection conexion;
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/sig115", "root", "root");

        System.out.println("Fecha inicial2:" + inicio);
        System.out.println("Fecha fin2:" + fin);
        System.out.println("producto2:" + marca);
        System.out.println("marca2:" + producto);

        //Se definen los parametros si es que el reporte necesita
        HashMap<String, Object> parameter = new HashMap<String, Object>();

        try {
            File file = new File(ruta);

            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

            httpServletResponse.setContentType("application/pdf");
            httpServletResponse.addHeader("Content-Type", "application/pdf");

            JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(file.getPath());
            parameter.put("idcentro", idcentro);
            parameter.put("producto", producto);
            parameter.put("marca", marca);
            parameter.put("inicio", inicio);
            parameter.put("fin", fin);

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, conexion);

            JRExporter jrExporter = null;
            jrExporter = new JRPdfExporter();
            jrExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            jrExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, httpServletResponse.getOutputStream());

            if (jrExporter != null) {
                try {
                    jrExporter.exportReport();
                } catch (JRException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conexion != null) {
                try {
                    conexion.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.reporte.bean;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author otoniel21
 */
public class ReporteTest {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Properties p = new Properties();
        p.load(new FileReader("src/main/resources/mariaDB.properties"));
        System.out.println("CADENA DE CONEXION:");
        String conexion ="jdbc:mysql://localhost:"+p.getProperty("PORT")+"/"+p.getProperty("DBNAME");
        System.out.println(conexion);
        System.out.println(p.getProperty("USER"));
        System.out.println(p.getProperty("PASSWORD"));
        System.out.println(p.getProperty("HOST"));

    }
}

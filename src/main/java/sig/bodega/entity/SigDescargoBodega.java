/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.entity;

import sig.bodega.entity.SigExistenciasBodega;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Gonzalez
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor



@Entity
@Table(name = "sig_descargo_bodega", catalog = "sig115")
public class SigDescargoBodega implements Serializable {

    public SigDescargoBodega(SigExistenciasBodega fk_ExistenciaBodega) {
        this.fk_ExistenciaBodega = fk_ExistenciaBodega;
    }
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "pk_Descargo_Bodega", unique = true, nullable = false)
    private int pk_DescargoBodega;
    
    @Column(name = "fk_producto", nullable = false, length = 19)
    private String fk_producto;
    
    @Column(name = "fk_centro", nullable = false)
    private int fk_centro;
    
    @Column(name = "cantidad", nullable = false )
    private int cantidad;
    
    @Column(name = "numAcuerdo", nullable = false, length = 8)
    private int numAcuerdo;
    
    @Column(name = "fecha_descargo", nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fecha_descargo;
    
    @Column(name = "realizado_por", nullable = false, length = 50)
    private String realizado_por;
    
    @Column(name = "justificacion", nullable = true, length = 50)
    private String justificacion;
    
    //@Column(name = "fk_IngresoBodega", nullable = true, length = 50)
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fk_Existencia_Bodega", nullable=false)
    private SigExistenciasBodega fk_ExistenciaBodega;
    
    @Column(name = "fecha_auditoria", nullable = false, length = 30)
    private String fecha_auditoria;

    @Override
    public String toString(){
        //String nom_centro;
        //nom_centro = String.valueOf(crudDAO.idMenu("select nombre from sig_centro where pk_idCentro="+fk_idCentro));
        return "Producto= "+fk_producto+", Cantidad= "+cantidad+", Fecha descargo= "+fecha_descargo+", Justificacion= "+justificacion+".";
    }
    
}

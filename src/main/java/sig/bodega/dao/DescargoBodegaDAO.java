/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.dao;

//import sig.login.entity.SigCatalogoMarca;
import sig.bodega.entity.SigDescargoBodega;
import sig.bodega.entity.SigExistenciasBodega;
import java.util.List;

/**
 *
 * @author Usuario
 */
public interface DescargoBodegaDAO {
    //Descargo
    public void guardarDescargo(SigDescargoBodega descargo);
    List<SigDescargoBodega> listDescargoBodega();
    
   
    
    //LISTADO Existencia
    public List<SigExistenciasBodega> listDetalleIngreso(int idCentro);
    public List<SigExistenciasBodega> listDetalleIngresoAll();

    public List<SigDescargoBodega> listaHistorialDescargos(int idCentro);
     
}

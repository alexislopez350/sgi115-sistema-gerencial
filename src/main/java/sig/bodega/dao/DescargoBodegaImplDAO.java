/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.dao;

import sig.dto.IngresoBodegaDto;
import sig.login.entity.SigCatalogoMarca;
import sig.bodega.entity.SigDescargoBodega;
import sig.bodega.entity.SigExistenciasBodega;
import sig.bodega.entity.SigSalidaBodega;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gonzalez
 */
@Transactional(value = "TMA")
@Service(value = "descargoBodegaDAO")
public class DescargoBodegaImplDAO extends HibernateDaoSupport implements DescargoBodegaDAO {

    @Autowired
    public DescargoBodegaImplDAO(@Qualifier(value = "SFA") SessionFactory sf) {
        super.setSessionFactory(sf);
    }

    @Override
    public void guardarDescargo(SigDescargoBodega descargo) {
        currentSession().persist(descargo);
    }

    @Override
    public List<SigDescargoBodega> listDescargoBodega() {
        return currentSession().createCriteria(SigDescargoBodega.class)
                .addOrder(Order.asc("pk_DescargoBodega"))
                .list();
    }

    @Override
    public List<SigExistenciasBodega> listDetalleIngreso(int idCentro) {
        return currentSession().createQuery("from SigExistenciasBodega e where e.fk_idCentro= " + idCentro + " and e.cantidad > 0").list();

    }

    @Override
    public List<SigExistenciasBodega> listDetalleIngresoAll() {
        return currentSession().createCriteria(SigExistenciasBodega.class)
                .addOrder(Order.desc("pk_ExistenciaBodega"))
                .list();
    }

    @Override
    public List<SigDescargoBodega> listaHistorialDescargos(int idCentro) {
        return currentSession().createQuery("from SigDescargoBodega d where d.fk_centro= " + idCentro +" ORDER BY d.pk_DescargoBodega DESC").list();
    }

}

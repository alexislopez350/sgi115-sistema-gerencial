/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.dao;

import sig.dto.IngresoBodegaDto;
import sig.dto.SalidaBodegaDto;
import sig.bodega.entity.SigExistenciasBodega;
import sig.bodega.entity.SigSalidaBodega;
import java.util.List;
import org.hibernate.SessionFactory;
import static org.hibernate.criterion.Expression.sql;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gonzalez
 */
@Transactional(value = "TMA")
@Service(value = "salidaDAO")
public class SalidaBodegaImplDAO extends HibernateDaoSupport implements SalidaBodegaDAO{
    
    @Autowired
    public SalidaBodegaImplDAO(@Qualifier(value = "SFA")SessionFactory sf) {
        super.setSessionFactory(sf);
    }

    @Override
    public void guardarSalida(SigSalidaBodega salida) {
        currentSession().persist(salida);
    }

    @Override
    public List<SigSalidaBodega> listSalidaBodega(int pkIdCentro) {
        return currentSession().createCriteria(SigSalidaBodega.class)
                .addOrder(Order.desc("pk_SalidaBodega"))
                .add(Restrictions.eq("fk_idCentro", pkIdCentro))
                .list();
    }

    @Override
    public List<SalidaBodegaDto> listProductoCentro(int idCentro) {
        return currentSession().createQuery("select new sig.dto.SalidaBodegaDto(i.pk_ExistenciaBodega, p.codigo, p.nombre, p.descrip, sum(i.cantidad), p.presentacion, u.nombre) "
						+ "from SigExistenciasBodega i inner join i.fk_codigo p "
                                                + "inner join p.sigUnidadmedida u "
						+ "where i.fk_idCentro=:id and i.cantidad > 0 "
						+ "group by i.fk_codigo").setParameter("id",idCentro).list();
    }

    @Override
    public List<SigExistenciasBodega> listDetalleProducto(int idCentro, String fk_codigo) {
    return currentSession().createQuery("from SigExistenciasBodega e where e.fk_idCentro= "+ idCentro +" and e.cantidad > 0 and e.pk_ExistenciaBodega= "+ fk_codigo).list();    
    }

    @Override
    public List<SigExistenciasBodega> connsultaPEPS(int idCentro) {
//        return currentSession().createQuery("select new sig.bodega.entity.SigExistenciasBodega(min(e.pk_ExistenciaBodega), e.precio_unit, e.cantidad, e.num_factura, e.realizado_por, e.tipo_ingreso,  e.nombre_distribuidor, e.descripcion, e.fecha_ingreso, e.fecha_caducidad, e.fk_idCentro, e.fk_codigo, e.fecha_auditoria, e.fk_marca) " +
//"               from SigExistenciasBodega e " +
//"               inner join e.fk_marca m " +
//"               inner join e.fk_codigo p " +
//"               inner join p.sigUnidadmedida u " +
//"               where e.fk_idCentro=:id and e.cantidad > 0 group by e.fk_codigo, m.nombre, p.sigUnidadmedida").setParameter("id",idCentro).list();
        return currentSession().createQuery("select new sig.bodega.entity.SigExistenciasBodega(min(e.pk_ExistenciaBodega), e.precio_unit, e.cantidad, e.num_factura, e.realizado_por, e.tipo_ingreso,  e.nombre_distribuidor, e.descripcion, e.fecha_ingreso, e.fecha_caducidad, e.fk_idCentro, e.fk_codigo, e.fecha_auditoria, e.fk_marca) " +
"               from SigExistenciasBodega e " +
"               inner join e.fk_marca m " +
"               inner join e.fk_codigo p " +
"               inner join p.sigUnidadmedida u " +
"               where e.fk_idCentro=:id and e.cantidad > 0 group by e.fk_codigo").setParameter("id",idCentro).list();

    }

    @Override
    public List<SigExistenciasBodega> listExistBodega() {
        return currentSession().createCriteria(SigExistenciasBodega.class)
                .addOrder(Order.asc("pk_ExistenciaBodega"))
                .list();
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.bo;

import sig.bodega.bean.DescargoBodegaBean;
import sig.bodega.dao.DescargoBodegaDAO;
import sig.login.dao.CrudDAO;
import sig.bodega.entity.SigDescargoBodega;
import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sig.bodega.dao.ExistenciaBodegaDAO;
import sig.bodega.entity.SigExistenciasBodega;
import sig.login.bean.SessionBean;

/**
 *
 * @author Usuario
 */
@Getter
@Service(value = "descargoBodegaBO")
public class DescargoBodegaImplBO implements DescargoBodegaBO {

    @Autowired
    private DescargoBodegaDAO descargoBodegaDao;
    @Autowired
    private ExistenciaBodegaDAO ingresoDao;
    @Autowired
    private CrudDAO crudDAO;

    @Override
    public boolean guardarDescargo(SigDescargoBodega descargo, SessionBean usu) {
        try {
            //ingreso.getPro().setEstado(true);
            //ingreso.getPro().setCantidad(0);
            crudDAO.insertObj(descargo);

            crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'"+usu.getUsu().getCodigo()+"', '"+usu.getUsu().getSigRol().getPkRol()+"', '"+usu.getUsu().getSigCentro().getPkIdCentro()+"','sig_decargo_bodega','DESCARGO', '"+ descargo.toString()+"')");

            int entrada = Integer.valueOf(crudDAO.scriptSQLKardex("SELECT ifnull(SUM(entrada),0) FROM sig_kardex_bodega WHERE fk_producto="+descargo.getFk_ExistenciaBodega().getFk_codigo().getCodigo()+""));
            int salida2 = Integer.valueOf(crudDAO.scriptSQLKardex("SELECT ifnull(SUM(salida),0) FROM sig_kardex_bodega WHERE fk_producto="+descargo.getFk_ExistenciaBodega().getFk_codigo().getCodigo()+""));
            int saldo = entrada-salida2-descargo.getCantidad();

            crudDAO.scriptSQL("insert into sig_kardex_bodega (fk_producto, fk_marca, fecha, precio_uni, salida, fk_centro, saldo, tipo_ingreso, num_factura) Values('"+descargo.getFk_ExistenciaBodega().getFk_codigo().getCodigo()+"',"+descargo.getFk_ExistenciaBodega().getFk_marca().getCodigoMarca()+", '"+new java.sql.Date(descargo.getFecha_descargo().getTime())+"', "+descargo.getFk_ExistenciaBodega().getPrecio_unit()+","+descargo.getCantidad()+", "+descargo.getFk_centro()+","+saldo+",'ACTA',"+descargo.getNumAcuerdo()+")");
            return true;
        } catch (Exception e) {
            System.out.println("** ingresoBodegaBO ** insert producto ** " + e);
            return false;
        }
    }

    @Override
    public List<SigDescargoBodega> listDescargoBodega() {
        return descargoBodegaDao.listDescargoBodega();
    }

    @Override
    public List<SigExistenciasBodega> listDetalleIngreso(int idCentro) {
        return descargoBodegaDao.listDetalleIngreso(idCentro);
    }

    @Override
    public List<SigExistenciasBodega> listDetalleIngresoAll() {
        return descargoBodegaDao.listDetalleIngresoAll();
    }

    @Override
    public List<SigDescargoBodega> listaHistorialDescargos(int idCentro) {
        return descargoBodegaDao.listaHistorialDescargos(idCentro);
    }

}

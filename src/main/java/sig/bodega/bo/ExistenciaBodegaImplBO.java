/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.bo;

import sig.bodega.bean.IngresoBodegaBean;
import sig.bodega.bean.MarcaBean;
import sig.dto.IngresoBodegaDto;
import sig.login.dao.CrudDAO;
import sig.login.entity.SigCatalogoMarca;
import sig.bodega.entity.SigExistenciasBodega;
import java.util.List;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sig.bodega.dao.ExistenciaBodegaDAO;
import sig.bodega.entity.SigIngresoAuditoria;
import sig.login.bean.SessionBean;

/**
 *
 * @author Mario
 */

@Getter
@Service(value = "existenciaBodegaBO")
public class ExistenciaBodegaImplBO implements ExistenciaBodegaBO {

    @Autowired
    private ExistenciaBodegaDAO existenciaDao;
    @Autowired
    private CrudDAO crudDAO;
    private SigIngresoAuditoria ingresoAuditable;

    @Override
    public boolean guardarIngreso(IngresoBodegaBean ingreso) {
        /*ingresoDao.guardarIngreso(ingreso);*/
        try {
            //ingreso.getPro().setEstado(true);
            //ingreso.getPro().setCantidad(0);
            crudDAO.insertObj(ingreso.getExistenciaBodega());
            return true;
        } catch (Exception e) {
            System.out.println("** ingresoBodegaBO ** insert producto ** " + e);
            return false;
        }
    }

    @Override
    public boolean guardarListaIngresos(IngresoBodegaBean ingreso, SessionBean usu) {
        try {
            List<SigExistenciasBodega> lista = ingreso.getListExistenciaBodega();
            for (SigExistenciasBodega existencias : lista) {
                crudDAO.insertObj(existencias);

                crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'"+usu.getUsu().getCodigo()+"', '"+usu.getUsu().getSigRol().getPkRol()+"', '"+usu.getUsu().getSigCentro().getPkIdCentro()+"','sig_existencia_bodega','INGRESO', '"+existencias.toString()+"')");

                int entrada = Integer.valueOf(crudDAO.scriptSQLKardex("SELECT ifnull(SUM(entrada),0) FROM sig_kardex_bodega WHERE fk_producto="+existencias.getFk_codigo().getCodigo()+""));
                int salida = Integer.valueOf(crudDAO.scriptSQLKardex("SELECT ifnull(SUM(salida),0) FROM sig_kardex_bodega WHERE fk_producto="+existencias.getFk_codigo().getCodigo()+""));
                int saldo = entrada-salida+existencias.getCantidad(); //estoy seguro que si quito esto funcion
                crudDAO.scriptSQL("insert into sig_kardex_bodega (fk_producto, fk_marca, fecha, precio_uni, entrada, fk_centro, saldo, tipo_ingreso, num_factura) Values('"+existencias.getFk_codigo().getCodigo()+"',"+existencias.getFk_marca().getCodigoMarca()+", '"+new java.sql.Date(existencias.getFecha_ingreso().getTime())+"', "+existencias.getPrecio_unit()+","+existencias.getCantidad()+", "+existencias.getFk_idCentro()+","+saldo+", '"+existencias.getTipo_ingreso()+"', "+existencias.getNum_factura()+")");
            }
            return true;
        } catch (Exception e) {
            System.out.println("Ha ocurrido un problema al guardar la lista de Existencias de bodega!");
            return false;
        }
    }

    @Override
    public List<SigExistenciasBodega> listExistenciaBodega() {
        return existenciaDao.listBodega();
    }

    @Override
    public List<IngresoBodegaDto> listFiltradaCentro(int idCentro) {
        return existenciaDao.listFiltradaCentro(idCentro);
    }

    @Override
    public List<SigCatalogoMarca> listMarca() {
        return existenciaDao.listMarca();
    }
    
    @Override
    public List<SigCatalogoMarca> listMarcaProBodega(String pro) {
        return existenciaDao.listMarcaProBodega(pro);
    }

    @Override
    public List<SigCatalogoMarca> listMarcaTodos() {
        return existenciaDao.listMarcaAll();
    }

    @Override
    public boolean insertMarca(MarcaBean bean, SessionBean usu) {
        try {
            bean.getMarca().setEstado(true);
            crudDAO.insertObj(bean.getMarca());
            crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'"+usu.getUsu().getCodigo()+"', '"+usu.getUsu().getSigRol().getPkRol()+"', "+usu.getUsu().getSigCentro().getPkIdCentro()+",'sig_catalogo_marca','INSERT', '"+ bean.getMarca().toString()+"')");
            return true;
        } catch (Exception e) {
            System.out.println("** IngresoMarcaImplBO ** insert marca ** " + e);
            return false;
        }
    }

    @Override
    public boolean updateMarca(SigCatalogoMarca marc, SessionBean usu) {
        try {
            crudDAO.updateObj(marc);
            crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'"+usu.getUsu().getCodigo()+"', '"+usu.getUsu().getSigRol().getPkRol()+"', "+usu.getUsu().getSigCentro().getPkIdCentro()+",'sig_existencia_bodega','UPDATE', '"+ marc.toString()+"')");
            return true;
        } catch (Exception e) {
            System.out.println("** IngresoMarcaImplBO ** update Marca ** " + e);
            return false;
        }
    }

    @Override
    public boolean deleteMarca(SigCatalogoMarca mrc) {
        try {
            crudDAO.deleteObj(mrc);
            return true;
        } catch (Exception e) {
            System.out.println("** AdminUsuarioImplBO ** deletePermiso menu ** " + e);
            return false;
        }
    }

    @Override
    public List<SigExistenciasBodega> listaExistenciaBodegaPorCentro(int pkIdCentro) {
        return existenciaDao.listaExistenciaBodegaPorCentro(pkIdCentro);
    }

    @Override
    public List<SigIngresoAuditoria> registrosAudCentro(int fk_idCentro) {
        return existenciaDao.registrosAudCentro(fk_idCentro);
    }

    @Override
    public boolean guardarIngAud(List<SigIngresoAuditoria> regPersistir) {
        try {
            for (SigIngresoAuditoria ingresos : regPersistir) {
                crudDAO.insertObj(ingresos);
            }
            return true;
        } catch (Exception e) {
            System.out.println("Ha ocurrido un problema al guardar la lista de Existencias de bodega! Por Auditoria");
            return false;
        }
    }
}

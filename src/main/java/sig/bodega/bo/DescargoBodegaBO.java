/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.bo;

import sig.bodega.entity.SigDescargoBodega;
import sig.bodega.entity.SigExistenciasBodega;
import sig.login.bean.SessionBean;
import java.util.List;

/**
 *
 * @author Usuario
 */
public interface DescargoBodegaBO {
    //DESCARGOOBODEGA
    List<SigDescargoBodega> listDescargoBodega();
    boolean guardarDescargo(SigDescargoBodega descargo, SessionBean usu);

    
//DetalleIngreso
    List <SigExistenciasBodega> listDetalleIngreso(int idCentro);
    List <SigExistenciasBodega> listDetalleIngresoAll();

    public List<SigDescargoBodega> listaHistorialDescargos(int idCentro);
}

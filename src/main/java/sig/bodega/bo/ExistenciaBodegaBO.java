/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.bo;

import sig.bodega.entity.SigExistenciasBodega;
import sig.bodega.bean.IngresoBodegaBean;
import sig.bodega.bean.MarcaBean;
import sig.dto.IngresoBodegaDto;
import sig.bodega.entity.SigIngresoAuditoria;
import sig.login.bean.SessionBean;
import sig.login.entity.SigCatalogoMarca;
import java.util.List;

/**
 *
 * @author Otoniel
 */
public interface ExistenciaBodegaBO {
    //INGRESOBODEGA
    List<SigExistenciasBodega> listExistenciaBodega();
    public List<IngresoBodegaDto> listFiltradaCentro(int idCentro);
    boolean guardarIngreso(IngresoBodegaBean ingreso);
    boolean guardarListaIngresos(IngresoBodegaBean ingreso, SessionBean usu);
    
    //CATALOGO DE MARCA
    List <SigCatalogoMarca> listMarca();
    List <SigCatalogoMarca> listMarcaProBodega(String producto);
    List <SigCatalogoMarca> listMarcaTodos();
    boolean insertMarca(MarcaBean bean, SessionBean usu);
    boolean updateMarca(SigCatalogoMarca marc, SessionBean usu);
    boolean deleteMarca(SigCatalogoMarca mrc);
//para obtener lo del centro:.
    public List<SigExistenciasBodega> listaExistenciaBodegaPorCentro(int pkIdCentro);

    public List<SigIngresoAuditoria> registrosAudCentro(int fk_idCentro);

    public boolean guardarIngAud(List<SigIngresoAuditoria> regPersistir);

}

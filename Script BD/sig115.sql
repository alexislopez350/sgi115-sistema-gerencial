-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.13-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para sig115
DROP DATABASE IF EXISTS `sig115`;
CREATE DATABASE IF NOT EXISTS `sig115` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `sig115`;

-- Volcando estructura para tabla sig115.sig_alimento
DROP TABLE IF EXISTS `sig_alimento`;
CREATE TABLE IF NOT EXISTS `sig_alimento` (
  `fk_menuciclico` int(11) NOT NULL,
  `semana` int(11) NOT NULL,
  `desLu` varchar(200) NOT NULL,
  `desMa` varchar(200) NOT NULL DEFAULT '',
  `desMi` varchar(200) NOT NULL DEFAULT '',
  `desJu` varchar(200) NOT NULL DEFAULT '',
  `desVi` varchar(200) NOT NULL DEFAULT '',
  `desSa` varchar(200) NOT NULL DEFAULT '',
  `desDo` varchar(200) NOT NULL DEFAULT '',
  `almLu` varchar(200) NOT NULL DEFAULT '',
  `almMa` varchar(200) NOT NULL DEFAULT '',
  `almMi` varchar(200) NOT NULL DEFAULT '',
  `almJu` varchar(200) NOT NULL DEFAULT '',
  `almVi` varchar(200) NOT NULL DEFAULT '',
  `almSa` varchar(200) NOT NULL DEFAULT '',
  `almDo` varchar(200) NOT NULL DEFAULT '',
  `cenLu` varchar(200) NOT NULL DEFAULT '',
  `cenMa` varchar(200) NOT NULL DEFAULT '',
  `cenMi` varchar(200) NOT NULL DEFAULT '',
  `cenJu` varchar(200) NOT NULL DEFAULT '',
  `cenVi` varchar(200) NOT NULL DEFAULT '',
  `cenSa` varchar(200) NOT NULL DEFAULT '',
  `cenDo` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`fk_menuciclico`,`semana`),
  KEY `fk_mcic_ali` (`fk_menuciclico`),
  CONSTRAINT `fk_mcic_ali` FOREIGN KEY (`fk_menuciclico`) REFERENCES `sig_menuciclico` (`pk_menuciclico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_alimento: ~0 rows (aproximadamente)
DELETE FROM `sig_alimento`;
/*!40000 ALTER TABLE `sig_alimento` DISABLE KEYS */;
/*!40000 ALTER TABLE `sig_alimento` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_beneficiario
DROP TABLE IF EXISTS `sig_beneficiario`;
CREATE TABLE IF NOT EXISTS `sig_beneficiario` (
  `PK_beneficiario` int(11) NOT NULL AUTO_INCREMENT,
  `PERSONAL` int(11) DEFAULT NULL,
  `HOMBRES` int(11) DEFAULT NULL,
  `MUJERES` int(11) DEFAULT NULL,
  `PNC` int(11) DEFAULT NULL,
  `NIÑOS` int(11) DEFAULT NULL,
  `NIÑAS` int(11) DEFAULT NULL,
  `ANCIANOS` int(11) DEFAULT NULL,
  `ANCIANAS` int(11) DEFAULT NULL,
  PRIMARY KEY (`PK_beneficiario`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_beneficiario: ~8 rows (aproximadamente)
DELETE FROM `sig_beneficiario`;
/*!40000 ALTER TABLE `sig_beneficiario` DISABLE KEYS */;
INSERT INTO `sig_beneficiario` (`PK_beneficiario`, `PERSONAL`, `HOMBRES`, `MUJERES`, `PNC`, `NIÑOS`, `NIÑAS`, `ANCIANOS`, `ANCIANAS`) VALUES
	(38, 1, 2, 3, 4, 5, 6, 7, 8),
	(39, 1, 2, 3, 4, 5, 6, 7, 8),
	(40, 1, 2, 3, 4, 5, 6, 7, 8),
	(41, 1, 2, 3, 4, 5, 6, 7, 8),
	(42, 1, 2, 3, 4, 5, 6, 7, 8),
	(43, 1, 2, 3, 4, 5, 6, 7, 8),
	(44, 1, 2, 3, 4, 5, 6, 7, 8),
	(45, 1, 2, 3, 4, 5, 6, 7, 8);
/*!40000 ALTER TABLE `sig_beneficiario` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_bitacora
DROP TABLE IF EXISTS `sig_bitacora`;
CREATE TABLE IF NOT EXISTS `sig_bitacora` (
  `pk_bitacora` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `fk_usuario` varchar(10) NOT NULL,
  `fk_rol` int(11) NOT NULL,
  `fk_centro` int(11) NOT NULL,
  `tabla` varchar(30) NOT NULL,
  `accion` varchar(20) NOT NULL,
  `informacion` varchar(800) NOT NULL,
  PRIMARY KEY (`pk_bitacora`),
  KEY `fk_usu_bit` (`fk_usuario`),
  KEY `fk_rol_bit` (`fk_rol`),
  KEY `fk_cen_bit` (`fk_centro`),
  CONSTRAINT `fk_cen_bit` FOREIGN KEY (`fk_centro`) REFERENCES `sig_centro` (`pk_centro`),
  CONSTRAINT `fk_rol_bit` FOREIGN KEY (`fk_rol`) REFERENCES `sig_rol` (`pk_rol`),
  CONSTRAINT `fk_usu_bit` FOREIGN KEY (`fk_usuario`) REFERENCES `sig_usuario` (`pk_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_bitacora: ~72 rows (aproximadamente)
DELETE FROM `sig_bitacora`;
/*!40000 ALTER TABLE `sig_bitacora` DISABLE KEYS */;
INSERT INTO `sig_bitacora` (`pk_bitacora`, `fecha`, `fk_usuario`, `fk_rol`, `fk_centro`, `tabla`, `accion`, `informacion`) VALUES
	(1, '2020-06-16 23:16:27', 'ROOT', 1, 1, 'sig_rol', 'INSERT', 'Rol= TACTICO, Estado= Activo.'),
	(2, '2020-06-16 23:16:45', 'ROOT', 1, 1, 'sig_usuario', 'INSERT', 'Codigo= YA20001, Centro= , Rol= TACTICO, Nombre= YY, Apellido= AA, Correo=, Estado= Activo.'),
	(3, '2020-06-17 02:10:02', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Nombre= null, Fecha= Wed Jun 17 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 1, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 139, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(4, '2020-06-17 04:17:43', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Nombre= null, Fecha= Wed Jun 17 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 2, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 140, Desayuno= null, Almuerzo= true, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(5, '2020-06-17 04:18:18', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Nombre= null, Fecha= Wed Jun 17 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 3, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 141, Desayuno= null, Almuerzo= null, Cena= true, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(6, '2020-06-17 04:18:34', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Nombre= null, Fecha= Wed Jun 17 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 4, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 142, Desayuno= null, Almuerzo= null, Cena= null, Refrigerio= true, tabuladorList= null.], Centro= Albergue 1.'),
	(7, '2020-06-17 05:30:56', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Nombre= null, Fecha= Mon Jun 15 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 5, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 143, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(8, '2020-06-17 05:31:57', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Nombre= null, Fecha= Mon Jun 15 00:00:00 CST 2020, Total= 53, ID Personal= [ID Beneficiario= 6, Personal= 7, Hombres= 6, Mujeres= 6, PNC= 7, Niños= 7, Niñas= 6, Ancianos= 7, Ancianas= 7, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 144, Desayuno= null, Almuerzo= true, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(9, '2020-06-17 05:32:19', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Nombre= null, Fecha= Mon Jun 15 00:00:00 CST 2020, Total= 41, ID Personal= [ID Beneficiario= 7, Personal= 5, Hombres= 6, Mujeres= 5, PNC= 5, Niños= 5, Niñas= 6, Ancianos= 4, Ancianas= 5, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 145, Desayuno= null, Almuerzo= null, Cena= true, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(10, '2020-06-17 05:33:09', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Nombre= null, Fecha= Mon Jun 15 00:00:00 CST 2020, Total= 45, ID Personal= [ID Beneficiario= 8, Personal= 5, Hombres= 6, Mujeres= 6, PNC= 7, Niños= 5, Niñas= 6, Ancianos= 5, Ancianas= 5, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 146, Desayuno= null, Almuerzo= null, Cena= null, Refrigerio= true, tabuladorList= null.], Centro= Albergue 1.'),
	(11, '2020-06-25 20:12:56', 'ROOT', 2, 1, 'sig_musuario', 'UPDATE', 'Codigo= YA20001, Centro= Albergue 1, Rol= TACTICO, Nombre= YY, Apellido= AA, Correo=, Estado= Activo.'),
	(12, '2020-06-28 22:07:11', 'ROOT', 2, 1, 'sig_rol', 'UPDATE', 'Rol= ADMINISTRADOR, Estado= Activo.'),
	(13, '2020-06-28 22:15:35', 'ROOT', 2, 1, 'sig_rol', 'INSERT', 'Rol= ESTRATEGICO, Estado= Activo.'),
	(14, '2020-06-30 01:29:29', 'ROOT', 2, 1, 'sig_producto', 'INSERT', 'Nombre= PRUEBA, Descripcion= PRUEBA, Presentacion= PRUEBA, TipoProducto= Perecedero, UnidadMedida= Prueba, Estado= Activo. '),
	(15, '2020-06-30 22:20:49', 'ROOT', 2, 1, 'sig_usuario', 'INSERT', 'Codigo= AB20001, Centro= Albergue 1, Rol= ADMINISTRADOR, Nombre= AA, Apellido= BB, Correo=alexislopez350@gmail.com, Estado= Activo.'),
	(16, '2020-06-30 22:41:01', 'ROOT', 2, 1, 'sig_existencia_bodega', 'INGRESO', 'Producto=SIN DETALLES, Marca= null, Descripcion= null, Estado= Inactivo., Precio unitario=10.0, Cantidad=10, Num factura=11, Realizado por=SUPER USUARIO, Tipo ingreso=NUMERO DE ENVIO, Nombre Distribuidor=11, Fecha ingreso=Mon Jun 08 00:00:00 CST 2020, Fecha caducidad=Mon Jun 08 00:00:00 CST 2020.'),
	(17, '2020-06-30 22:41:02', 'ROOT', 2, 1, 'sig_existencia_bodega', 'INGRESO', 'Producto=SIN DETALLES, Marca= null, Descripcion= null, Estado= Inactivo., Precio unitario=10.0, Cantidad=10, Num factura=11, Realizado por=SUPER USUARIO, Tipo ingreso=NUMERO DE ENVIO, Nombre Distribuidor=11, Fecha ingreso=Mon Jun 08 00:00:00 CST 2020, Fecha caducidad=Mon Jun 08 00:00:00 CST 2020.'),
	(18, '2020-06-30 23:26:42', 'ROOT', 2, 1, 'sig_salida_bodega', 'SALIDA', 'Producto: 1, Cantidad= 1, Fecha salida: Wed Jun 03 00:00:00 CST 2020, Entregado a: 111, Justificacion= SIN OBSERVACION.'),
	(19, '2020-06-30 23:29:32', 'ROOT', 2, 1, 'sig_salida_bodega', 'SALIDA', 'Producto: 1, Cantidad= 1, Fecha salida: Wed Jun 10 00:00:00 CST 2020, Entregado a: 1, Justificacion= SIN OBSERVACION.'),
	(20, '2020-06-30 23:34:55', 'ROOT', 2, 1, 'sig_decargo_bodega', 'DESCARGO', 'Producto= 1, Cantidad= 2, Fecha descargo= Tue Jun 30 23:34:30 CST 2020, Justificacion= YO QUERO.'),
	(21, '2020-06-30 23:38:27', 'ROOT', 2, 1, 'sig_decargo_bodega', 'DESCARGO', 'Producto= 1, Cantidad= 1, Fecha descargo= Tue Jun 30 23:38:10 CST 2020, Justificacion= 1.'),
	(22, '2020-06-30 23:38:57', 'ROOT', 2, 1, 'sig_decargo_bodega', 'DESCARGO', 'Producto= 1, Cantidad= 2, Fecha descargo= Tue Jun 30 23:38:35 CST 2020, Justificacion= 11.'),
	(23, '2020-06-30 23:39:31', 'ROOT', 2, 1, 'sig_decargo_bodega', 'DESCARGO', 'Producto= 1, Cantidad= 1, Fecha descargo= Tue Jun 30 23:39:11 CST 2020, Justificacion= DFDF.'),
	(24, '2020-06-30 23:42:17', 'ROOT', 2, 1, 'sig_decargo_bodega', 'DESCARGO', 'Producto= 1, Cantidad= 2, Fecha descargo= Tue Jun 30 23:42:07 CST 2020, Justificacion= 123.'),
	(25, '2020-06-30 23:43:06', 'ROOT', 2, 1, 'sig_decargo_bodega', 'DESCARGO', 'Producto= 150000000000001, Cantidad= 3, Fecha descargo= Tue Jun 30 23:42:56 CST 2020, Justificacion= 10.'),
	(26, '2020-06-30 23:43:23', 'ROOT', 2, 1, 'sig_decargo_bodega', 'DESCARGO', 'Producto= 150000000000001, Cantidad= 2, Fecha descargo= Tue Jun 30 23:43:07 CST 2020, Justificacion= 10.'),
	(27, '2020-06-30 23:45:06', 'ROOT', 2, 1, 'sig_decargo_bodega', 'DESCARGO', 'Producto= 1, Cantidad= 8, Fecha descargo= Tue Jun 30 23:43:25 CST 2020, Justificacion= ASD.'),
	(28, '2020-06-30 23:51:20', 'ROOT', 2, 1, 'sig_decargo_bodega', 'DESCARGO', 'Producto= 1, Cantidad= 2, Fecha descargo= Tue Jun 30 23:51:12 CST 2020, Justificacion= 1.'),
	(29, '2020-07-01 00:13:47', 'ROOT', 2, 1, 'sig_existencia_bodega', 'INGRESO', 'Producto=SIN DETALLES, Marca= null, Descripcion= null, Estado= Inactivo., Precio unitario=10.0, Cantidad=11, Num factura=11, Realizado por=SUPER USUARIO, Tipo ingreso=NOTA DE REMISION, Nombre Distribuidor=11, Fecha ingreso=Wed Jul 01 00:00:00 CST 2020, Fecha caducidad=Wed Jul 01 00:00:00 CST 2020.'),
	(30, '2020-07-01 00:34:46', 'ROOT', 2, 1, 'sig_rol', 'INSERT', 'Rol= LABORATORISTA, Estado= Activo.'),
	(31, '2020-07-01 00:35:04', 'ROOT', 2, 1, 'sig_usuario', 'INSERT', 'Codigo= AA20001, Centro= Albergue 1, Rol= LABORATORISTA, Nombre= AA, Apellido= AA, Correo=alexislopez350@gmail.com, Estado= Activo.'),
	(32, '2020-07-01 00:35:33', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Nombre= null, Fecha= Wed Jul 01 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 9, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 147, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(33, '2020-07-01 00:36:17', 'ROOT', 2, 1, 'sig_existencia_bodega', 'INGRESO', 'Producto=SIN DETALLES, Marca= null, Descripcion= null, Estado= Inactivo., Precio unitario=10.0, Cantidad=1, Num factura=12, Realizado por=SUPER USUARIO, Tipo ingreso=NOTA DE REMISION, Nombre Distribuidor=12, Fecha ingreso=Wed Jul 01 00:00:00 CST 2020, Fecha caducidad=Wed Jul 01 00:00:00 CST 2020.'),
	(34, '2020-07-01 00:36:29', 'ROOT', 2, 1, 'sig_producto', 'INSERT', 'Nombre= PRUEBA, Descripcion= PRUEBA, Presentacion= PRUEBA, TipoProducto= Perecedero, UnidadMedida= Prueba, Estado= Activo. '),
	(35, '2020-07-01 00:36:56', 'ROOT', 2, 1, 'sig_salida_bodega', 'SALIDA', 'Producto: 150000000000001, Cantidad= 10, Fecha salida: Wed Jul 01 00:00:00 CST 2020, Entregado a: 11, Justificacion= SIN OBSERVACION.'),
	(36, '2020-07-01 00:37:13', 'ROOT', 2, 1, 'sig_decargo_bodega', 'DESCARGO', 'Producto= 150000000000001, Cantidad= 1, Fecha descargo= Wed Jul 01 00:36:59 CST 2020, Justificacion= 1.'),
	(37, '2020-07-02 04:19:56', 'ROOT', 2, 1, 'sig_usuario', 'INSERT', 'Codigo= AA20002, Centro= Albergue 1, Rol= ESTRATEGICO, Nombre= A, Apellido= A, Correo=alexislopez350@gmail.com, Estado= Activo.'),
	(38, '2020-07-02 05:47:45', 'ROOT', 2, 1, 'sig_rol', 'INSERT', 'Rol= TACTICO, Estado= Activo.'),
	(39, '2020-07-02 05:57:06', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 12, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 150, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(40, '2020-07-02 06:00:02', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 13, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 151, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(41, '2020-07-02 06:08:22', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 14, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 152, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(42, '2020-07-02 06:13:03', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 15, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 153, Desayuno= null, Almuerzo= null, Cena= null, Refrigerio= true, tabuladorList= null.], Centro= Albergue 1.'),
	(43, '2020-07-02 06:30:21', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 7, ID Personal= [ID Beneficiario= 16, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 0, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 154, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(44, '2020-07-02 13:03:15', 'ROOT', 2, 1, 'sig_tabulador', 'INSERT', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 17, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 155, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(45, '2020-07-02 22:03:26', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 8, ID Personal= [ID Beneficiario= 18, Personal= 1, Hombres= 1, Mujeres= 1, PNC= 1, Niños= 1, Niñas= 1, Ancianos= 1, Ancianas= 1, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 156, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(46, '2020-07-02 22:06:24', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 19, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 157, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(47, '2020-07-02 22:07:16', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 20, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 158, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(48, '2020-07-02 22:08:35', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 21, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 159, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(49, '2020-07-02 22:25:34', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 22, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 160, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(50, '2020-07-02 22:25:34', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 23, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 161, Desayuno= null, Almuerzo= true, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(51, '2020-07-02 22:25:34', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 24, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 162, Desayuno= null, Almuerzo= null, Cena= true, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(52, '2020-07-02 22:25:34', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 25, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 163, Desayuno= null, Almuerzo= null, Cena= null, Refrigerio= true, tabuladorList= null.], Centro= Albergue 1.'),
	(53, '2020-07-02 22:30:30', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 26, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 164, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(54, '2020-07-02 22:30:30', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 27, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 165, Desayuno= null, Almuerzo= true, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(55, '2020-07-02 22:30:30', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 28, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 166, Desayuno= null, Almuerzo= null, Cena= true, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(56, '2020-07-02 22:30:30', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 29, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 167, Desayuno= null, Almuerzo= null, Cena= null, Refrigerio= true, tabuladorList= null.], Centro= Albergue 1.'),
	(57, '2020-07-02 22:31:13', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 30, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 168, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(58, '2020-07-02 22:31:13', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 31, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 169, Desayuno= null, Almuerzo= true, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(59, '2020-07-02 22:31:13', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 32, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 170, Desayuno= null, Almuerzo= null, Cena= true, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(60, '2020-07-02 22:31:13', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 72, ID Personal= [ID Beneficiario= 33, Personal= 9, Hombres= 9, Mujeres= 9, PNC= 9, Niños= 9, Niñas= 9, Ancianos= 9, Ancianas= 9, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 171, Desayuno= null, Almuerzo= null, Cena= null, Refrigerio= true, tabuladorList= null.], Centro= Albergue 1.'),
	(61, '2020-07-02 22:34:44', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 34, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 172, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(62, '2020-07-02 22:34:44', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 35, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 173, Desayuno= null, Almuerzo= true, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(63, '2020-07-02 22:34:44', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 36, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 174, Desayuno= null, Almuerzo= null, Cena= true, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(64, '2020-07-02 22:34:44', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 37, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 175, Desayuno= null, Almuerzo= null, Cena= null, Refrigerio= true, tabuladorList= null.], Centro= Albergue 1.'),
	(65, '2020-07-02 22:36:37', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 38, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 176, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(66, '2020-07-02 22:36:37', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 39, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 177, Desayuno= null, Almuerzo= true, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(67, '2020-07-02 22:36:37', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 40, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 178, Desayuno= null, Almuerzo= null, Cena= true, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(68, '2020-07-02 22:36:37', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Thu Jul 02 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 41, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 179, Desayuno= null, Almuerzo= null, Cena= null, Refrigerio= true, tabuladorList= null.], Centro= Albergue 1.'),
	(69, '2020-07-02 22:36:37', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Fri Jul 03 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 42, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 180, Desayuno= true, Almuerzo= null, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(70, '2020-07-02 22:36:37', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Fri Jul 03 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 43, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 181, Desayuno= null, Almuerzo= true, Cena= null, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(71, '2020-07-02 22:36:37', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Fri Jul 03 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 44, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 182, Desayuno= null, Almuerzo= null, Cena= true, Refrigerio= null, tabuladorList= null.], Centro= Albergue 1.'),
	(72, '2020-07-02 22:36:38', 'ROOT', 2, 1, 'sig_tabulador', 'Archivo', 'Fecha= Fri Jul 03 00:00:00 CST 2020, Total= 36, ID Personal= [ID Beneficiario= 45, Personal= 1, Hombres= 2, Mujeres= 3, PNC= 4, Niños= 5, Niñas= 6, Ancianos= 7, Ancianas= 8, tabuladorList= null.], ID Tiempo comida= [ID Tiempo comida= 183, Desayuno= null, Almuerzo= null, Cena= null, Refrigerio= true, tabuladorList= null.], Centro= Albergue 1.');
/*!40000 ALTER TABLE `sig_bitacora` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_centro
DROP TABLE IF EXISTS `sig_centro`;
CREATE TABLE IF NOT EXISTS `sig_centro` (
  `pk_centro` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `municipio` varchar(30) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `departamento` varchar(30) NOT NULL,
  PRIMARY KEY (`pk_centro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_centro: ~0 rows (aproximadamente)
DELETE FROM `sig_centro`;
/*!40000 ALTER TABLE `sig_centro` DISABLE KEYS */;
INSERT INTO `sig_centro` (`pk_centro`, `nombre`, `telefono`, `estado`, `municipio`, `direccion`, `departamento`) VALUES
	(1, 'Albergue 1', '77777777', 1, 'Manguito', 'El Manguito', 'El Manguito');
/*!40000 ALTER TABLE `sig_centro` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_descargo_bodega
DROP TABLE IF EXISTS `sig_descargo_bodega`;
CREATE TABLE IF NOT EXISTS `sig_descargo_bodega` (
  `pk_Descargo_Bodega` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL,
  `justificacion` varchar(50) DEFAULT NULL,
  `fecha_auditoria` varchar(30) NOT NULL,
  `fecha_descargo` date NOT NULL,
  `fk_existencia_bodega` int(11) DEFAULT NULL,
  `fk_centro` int(11) NOT NULL,
  `fk_producto` varchar(19) NOT NULL,
  `realizado_por` varchar(50) NOT NULL,
  `numAcuerdo` int(11) NOT NULL,
  PRIMARY KEY (`pk_Descargo_Bodega`),
  KEY `fk_descargoB_existenciaB` (`fk_existencia_bodega`),
  KEY `FK_descargoB_producto` (`fk_producto`),
  KEY `FK_descargoB_centro` (`fk_centro`),
  CONSTRAINT `fk_descargoB_centro` FOREIGN KEY (`fk_centro`) REFERENCES `sig_centro` (`pk_centro`),
  CONSTRAINT `fk_descargoB_existenciaB` FOREIGN KEY (`fk_existencia_bodega`) REFERENCES `sig_existencia_bodega` (`pk_Existencia_Bodega`),
  CONSTRAINT `fk_descargoB_producto` FOREIGN KEY (`fk_producto`) REFERENCES `sig_producto` (`pk_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_descargo_bodega: ~0 rows (aproximadamente)
DELETE FROM `sig_descargo_bodega`;
/*!40000 ALTER TABLE `sig_descargo_bodega` DISABLE KEYS */;
INSERT INTO `sig_descargo_bodega` (`pk_Descargo_Bodega`, `cantidad`, `justificacion`, `fecha_auditoria`, `fecha_descargo`, `fk_existencia_bodega`, `fk_centro`, `fk_producto`, `realizado_por`, `numAcuerdo`) VALUES
	(13, 1, '1', '07-01-2020', '2020-07-01', 86, 1, '150000000000001', 'SUPER USUARIO', 1);
/*!40000 ALTER TABLE `sig_descargo_bodega` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_existencia_bodega
DROP TABLE IF EXISTS `sig_existencia_bodega`;
CREATE TABLE IF NOT EXISTS `sig_existencia_bodega` (
  `pk_Existencia_Bodega` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL,
  `fecha_auditoria` varchar(30) NOT NULL DEFAULT '',
  `fecha_caducidad` date NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `fk_centro` int(11) NOT NULL,
  `nombre_distribuidor` varchar(50) NOT NULL,
  `num_factura` int(11) NOT NULL,
  `precio_unit` double NOT NULL,
  `realizado_por` varchar(50) NOT NULL,
  `tipo_ingreso` varchar(50) NOT NULL,
  `fk_producto` varchar(19) NOT NULL,
  `fk_marca` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`pk_Existencia_Bodega`),
  KEY `FK_existenciaB_producto` (`fk_producto`),
  KEY `FK_existenciaB_marca` (`fk_marca`),
  KEY `FK_existenciaB_centro` (`fk_centro`),
  CONSTRAINT `FK_existenciaB_centro` FOREIGN KEY (`fk_centro`) REFERENCES `sig_centro` (`pk_centro`),
  CONSTRAINT `FK_existenciaB_marca` FOREIGN KEY (`fk_marca`) REFERENCES `sig_marca` (`pk_marca`),
  CONSTRAINT `FK_existenciaB_producto` FOREIGN KEY (`fk_producto`) REFERENCES `sig_producto` (`pk_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_existencia_bodega: ~2 rows (aproximadamente)
DELETE FROM `sig_existencia_bodega`;
/*!40000 ALTER TABLE `sig_existencia_bodega` DISABLE KEYS */;
INSERT INTO `sig_existencia_bodega` (`pk_Existencia_Bodega`, `cantidad`, `fecha_auditoria`, `fecha_caducidad`, `fecha_ingreso`, `fk_centro`, `nombre_distribuidor`, `num_factura`, `precio_unit`, `realizado_por`, `tipo_ingreso`, `fk_producto`, `fk_marca`, `descripcion`) VALUES
	(85, 1, 'asdfasd', '2020-07-01', '2020-07-01', 1, '11', 11, 10, 'SUPER USUARIO', 'NOTA DE REMISION', '150000000000001', 1, 'SIN DETALLES'),
	(86, 0, 'asdfasd', '2020-07-01', '2020-07-01', 1, '12', 12, 10, 'SUPER USUARIO', 'NOTA DE REMISION', '150000000000001', 1, 'SIN DETALLES');
/*!40000 ALTER TABLE `sig_existencia_bodega` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_icono
DROP TABLE IF EXISTS `sig_icono`;
CREATE TABLE IF NOT EXISTS `sig_icono` (
  `pk_icono` varchar(50) NOT NULL,
  PRIMARY KEY (`pk_icono`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_icono: ~3 rows (aproximadamente)
DELETE FROM `sig_icono`;
/*!40000 ALTER TABLE `sig_icono` DISABLE KEYS */;
INSERT INTO `sig_icono` (`pk_icono`) VALUES
	('FA-HOME'),
	('FA-SERVER'),
	('FA-USER');
/*!40000 ALTER TABLE `sig_icono` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_ingresob_aud
DROP TABLE IF EXISTS `sig_ingresob_aud`;
CREATE TABLE IF NOT EXISTS `sig_ingresob_aud` (
  `pk_ingresoB_aud` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad_aud` int(11) NOT NULL,
  `descripcion_aud` varchar(50) NOT NULL,
  `fecha_caducidad_aud` datetime DEFAULT NULL,
  `fecha_ingreso_aud` datetime DEFAULT NULL,
  `fecha_registro_aud` datetime NOT NULL,
  `fk_centro` int(11) NOT NULL,
  `nombre_distribuidor_aud` varchar(50) NOT NULL,
  `num_factura_aud` int(11) NOT NULL,
  `precio_unit_aud` double NOT NULL,
  `realizado_por_aud` varchar(50) NOT NULL,
  `tipo_ingreso_aud` varchar(20) NOT NULL,
  `fk_producto` varchar(19) NOT NULL,
  `fk_marca` int(11) NOT NULL,
  PRIMARY KEY (`pk_ingresoB_aud`),
  KEY `FK_ingresoBAud_producto` (`fk_producto`),
  KEY `FK_ingresoBAud_marca` (`fk_marca`),
  KEY `FK_ingresoBAud_centro` (`fk_centro`),
  CONSTRAINT `FK_ingresoBAud_centro` FOREIGN KEY (`fk_centro`) REFERENCES `sig_centro` (`pk_centro`),
  CONSTRAINT `FK_ingresoBAud_marca` FOREIGN KEY (`fk_marca`) REFERENCES `sig_marca` (`pk_marca`),
  CONSTRAINT `FK_ingresoBAud_producto` FOREIGN KEY (`fk_producto`) REFERENCES `sig_producto` (`pk_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_ingresob_aud: ~8 rows (aproximadamente)
DELETE FROM `sig_ingresob_aud`;
/*!40000 ALTER TABLE `sig_ingresob_aud` DISABLE KEYS */;
INSERT INTO `sig_ingresob_aud` (`pk_ingresoB_aud`, `cantidad_aud`, `descripcion_aud`, `fecha_caducidad_aud`, `fecha_ingreso_aud`, `fecha_registro_aud`, `fk_centro`, `nombre_distribuidor_aud`, `num_factura_aud`, `precio_unit_aud`, `realizado_por_aud`, `tipo_ingreso_aud`, `fk_producto`, `fk_marca`) VALUES
	(20, 1, 'SIN DETALLES', '2020-06-08 00:00:00', '2020-06-08 00:00:00', '2020-06-08 00:00:00', 1, '12', 12, 10, 'SUPER USUARIO', 'NUMERO DE ENVIO', '1', 1),
	(21, 1, 'SIN DETALLES', '2020-06-08 00:00:00', '2020-06-08 00:00:00', '2020-06-08 00:00:00', 1, '12', 12, 0.1, 'SUPER USUARIO', 'NUMERO DE ENVIO', '150000000000001', 1),
	(22, 23, 'SIN DETALLES', '2020-06-09 00:00:00', '2020-06-09 00:00:00', '2020-06-09 00:00:00', 1, 'DDD', 111, 22, 'SUPER USUARIO', 'ACTA', '150000000000001', 2),
	(23, 1, 'SIN DETALLES', '2020-06-09 00:00:00', '2020-06-09 00:00:00', '2020-06-09 00:00:00', 1, 'AS', 12, 10, 'SUPER USUARIO', 'NUMERO DE ENVIO', '150000000000001', 1),
	(24, 10, 'SIN DETALLES', '2020-06-08 00:00:00', '2020-06-08 00:00:00', '2020-06-08 00:00:00', 1, '11', 11, 10, 'SUPER USUARIO', 'NUMERO DE ENVIO', '150000000000001', 1),
	(25, 10, 'SIN DETALLES', '2020-06-08 00:00:00', '2020-06-08 00:00:00', '2020-06-08 00:00:00', 1, '11', 11, 10, 'SUPER USUARIO', 'NUMERO DE ENVIO', '1', 1),
	(26, 11, 'SIN DETALLES', '2020-07-01 00:00:00', '2020-07-01 00:00:00', '2020-07-01 00:00:00', 1, '11', 11, 10, 'SUPER USUARIO', 'NOTA DE REMISION', '150000000000001', 1),
	(27, 1, 'SIN DETALLES', '2020-07-01 00:00:00', '2020-07-01 00:00:00', '2020-07-01 00:00:00', 1, '12', 12, 10, 'SUPER USUARIO', 'NOTA DE REMISION', '150000000000001', 1);
/*!40000 ALTER TABLE `sig_ingresob_aud` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_kardex_bodega
DROP TABLE IF EXISTS `sig_kardex_bodega`;
CREATE TABLE IF NOT EXISTS `sig_kardex_bodega` (
  `fk_producto` varchar(19) DEFAULT NULL,
  `fk_marca` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `tipo_ingreso` varchar(50) DEFAULT '---',
  `num_factura` int(11) DEFAULT 0,
  `precio_uni` double DEFAULT NULL,
  `entrada` int(5) DEFAULT 0,
  `salida` int(5) DEFAULT 0,
  `saldo` int(5) DEFAULT 0,
  `fk_centro` int(11) DEFAULT NULL,
  KEY `fk_kardexB_producto` (`fk_producto`),
  KEY `FK_kardexB_centro` (`fk_centro`),
  KEY `FK_kardexB_marca` (`fk_marca`),
  CONSTRAINT `FK_kardexB_centro` FOREIGN KEY (`fk_centro`) REFERENCES `sig_centro` (`pk_centro`),
  CONSTRAINT `FK_kardexB_marca` FOREIGN KEY (`fk_marca`) REFERENCES `sig_marca` (`pk_marca`),
  CONSTRAINT `fk_kardexB_producto` FOREIGN KEY (`fk_producto`) REFERENCES `sig_producto` (`pk_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_kardex_bodega: ~4 rows (aproximadamente)
DELETE FROM `sig_kardex_bodega`;
/*!40000 ALTER TABLE `sig_kardex_bodega` DISABLE KEYS */;
INSERT INTO `sig_kardex_bodega` (`fk_producto`, `fk_marca`, `fecha`, `tipo_ingreso`, `num_factura`, `precio_uni`, `entrada`, `salida`, `saldo`, `fk_centro`) VALUES
	('150000000000001', 1, '2020-07-01', 'NOTA DE REMISION', 11, 10, 11, 0, 11, 1),
	('150000000000001', 1, '2020-07-01', 'NOTA DE REMISION', 12, 10, 1, 0, 12, 1),
	('150000000000001', 1, '2020-07-01', '---', 0, 10, 0, 10, 2, 1),
	('150000000000001', 1, '2020-07-01', 'ACTA', 1, 10, 0, 1, 1, 1);
/*!40000 ALTER TABLE `sig_kardex_bodega` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_marca
DROP TABLE IF EXISTS `sig_marca`;
CREATE TABLE IF NOT EXISTS `sig_marca` (
  `pk_marca` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) DEFAULT NULL,
  `estado` bit(1) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  PRIMARY KEY (`pk_marca`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_marca: ~3 rows (aproximadamente)
DELETE FROM `sig_marca`;
/*!40000 ALTER TABLE `sig_marca` DISABLE KEYS */;
INSERT INTO `sig_marca` (`pk_marca`, `descripcion`, `estado`, `nombre`) VALUES
	(0, 'SIN MARCA', b'1', 'SIN MARCA'),
	(1, 'PRUEBA', b'1', 'PRUEBA'),
	(2, 'prueba 2', b'1', 'prueba 2');
/*!40000 ALTER TABLE `sig_marca` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_menu
DROP TABLE IF EXISTS `sig_menu`;
CREATE TABLE IF NOT EXISTS `sig_menu` (
  `pk_menu` int(11) NOT NULL,
  `ruta_raiz` varchar(250) DEFAULT NULL,
  `nom_archivo` varchar(250) DEFAULT NULL,
  `nom_vista` varchar(70) DEFAULT NULL,
  `fk_icono` varchar(50) DEFAULT NULL,
  `fk_menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk_menu`),
  KEY `fk_menu_menu` (`fk_menu`),
  KEY `fk_menu_icono` (`fk_icono`),
  CONSTRAINT `fk_menu_icono` FOREIGN KEY (`fk_icono`) REFERENCES `sig_icono` (`pk_icono`),
  CONSTRAINT `fk_menu_menu` FOREIGN KEY (`fk_menu`) REFERENCES `sig_menu` (`pk_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_menu: ~14 rows (aproximadamente)
DELETE FROM `sig_menu`;
/*!40000 ALTER TABLE `sig_menu` DISABLE KEYS */;
INSERT INTO `sig_menu` (`pk_menu`, `ruta_raiz`, `nom_archivo`, `nom_vista`, `fk_icono`, `fk_menu`) VALUES
	(1, '/secure/Administrador/', 'Inicio', 'Inicio', 'FA-HOME', NULL),
	(2, '/secure/Administrador/', 'Rol', 'Administracion de Roles', 'FA-SERVER', NULL),
	(3, '/secure/Administrador/', 'Usuario', 'Administracion de Usuario', 'FA-USER', NULL),
	(4, '/secure/Raciones/', 'prueba', 'Ingreso de Raciones', 'FA-SERVER', NULL),
	(5, '/secure/Raciones/', 'IngresoExcelRacion', 'Ingreso de Raciones', 'FA-USER', NULL),
	(6, '/secure/Reportes/', 'ReporteRaciones', 'Reporte Raciones', 'FA-HOME', NULL),
	(7, '/secure/Reportes/', 'ReporteExistenciaConsumo', 'Reporte Existencias', 'FA-USER', NULL),
	(8, '/secure/Bodega/', 'IngresoBodega', 'Ingreso Bodega', 'FA-USER', NULL),
	(9, '/secure/Administrador/', 'Producto', 'Producto', 'FA-SERVER', NULL),
	(10, '/secure/Reportes/', 'ReporteUsuarios', 'Reporte Usuarios', 'FA-HOME', NULL),
	(11, '/secure/Bodega/', 'SalidaBodega', 'Egreso Bodega', 'FA-HOME', NULL),
	(12, '/secure/Bodega/', 'DescargoBodega', 'Descargo Bodega', 'FA-HOME', NULL),
	(13, '/secure/Reportes/', 'ReporteKardexBodega', 'Kardex Bodega', 'FA-HOME', NULL),
	(14, '/secure/Reportes/', 'ReporteBitacora', 'Reporte Bitacora', 'FA-HOME', NULL);
/*!40000 ALTER TABLE `sig_menu` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_menu_ciclico
DROP TABLE IF EXISTS `sig_menu_ciclico`;
CREATE TABLE IF NOT EXISTS `sig_menu_ciclico` (
  `pk_menu_ciclico` int(11) NOT NULL AUTO_INCREMENT,
  `ANIO` int(11) NOT NULL DEFAULT 0,
  `INICIO` date DEFAULT NULL,
  `FIN` date NOT NULL,
  `ESTADO` tinyint(4) NOT NULL DEFAULT 0,
  `fk_centro` int(11) NOT NULL,
  PRIMARY KEY (`pk_menu_ciclico`),
  KEY `fk_cenC_centro` (`fk_centro`),
  CONSTRAINT `fk_menuC_centro` FOREIGN KEY (`fk_centro`) REFERENCES `sig_centro` (`pk_centro`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_menu_ciclico: ~0 rows (aproximadamente)
DELETE FROM `sig_menu_ciclico`;
/*!40000 ALTER TABLE `sig_menu_ciclico` DISABLE KEYS */;
/*!40000 ALTER TABLE `sig_menu_ciclico` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_parametro
DROP TABLE IF EXISTS `sig_parametro`;
CREATE TABLE IF NOT EXISTS `sig_parametro` (
  `pk_parametro` int(11) NOT NULL,
  `valor` varchar(250) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  PRIMARY KEY (`pk_parametro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_parametro: ~0 rows (aproximadamente)
DELETE FROM `sig_parametro`;
/*!40000 ALTER TABLE `sig_parametro` DISABLE KEYS */;
/*!40000 ALTER TABLE `sig_parametro` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_permiso
DROP TABLE IF EXISTS `sig_permiso`;
CREATE TABLE IF NOT EXISTS `sig_permiso` (
  `fk_menu` int(11) NOT NULL,
  `fk_rol` int(11) NOT NULL,
  `orden` smallint(6) NOT NULL,
  PRIMARY KEY (`fk_rol`,`fk_menu`),
  KEY `fk_permiso_menu` (`fk_menu`),
  CONSTRAINT `fk_permiso_menu` FOREIGN KEY (`fk_menu`) REFERENCES `sig_menu` (`pk_menu`),
  CONSTRAINT `fk_permiso_rol` FOREIGN KEY (`fk_rol`) REFERENCES `sig_rol` (`pk_rol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_permiso: ~16 rows (aproximadamente)
DELETE FROM `sig_permiso`;
/*!40000 ALTER TABLE `sig_permiso` DISABLE KEYS */;
INSERT INTO `sig_permiso` (`fk_menu`, `fk_rol`, `orden`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 3),
	(1, 2, 1),
	(2, 2, 2),
	(3, 2, 3),
	(5, 2, 4),
	(6, 2, 5),
	(7, 2, 6),
	(8, 2, 7),
	(9, 2, 8),
	(10, 2, 9),
	(11, 2, 10),
	(12, 2, 11),
	(13, 2, 12),
	(14, 2, 13);
/*!40000 ALTER TABLE `sig_permiso` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_producto
DROP TABLE IF EXISTS `sig_producto`;
CREATE TABLE IF NOT EXISTS `sig_producto` (
  `pk_producto` varchar(19) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descrip` varchar(200) NOT NULL,
  `fk_unidadMedida` int(11) NOT NULL,
  `presentacion` varchar(50) NOT NULL,
  `fk_tipoProducto` int(11) NOT NULL,
  `fk_rubro` int(7) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`pk_producto`),
  KEY `fk_pro_rubro` (`fk_rubro`),
  KEY `fk_pro_tipo` (`fk_tipoProducto`),
  KEY `fk_pro_unidad` (`fk_unidadMedida`),
  CONSTRAINT `fk_pro_rubro` FOREIGN KEY (`fk_rubro`) REFERENCES `sig_rubro` (`pk_rubro`),
  CONSTRAINT `fk_pro_tipo` FOREIGN KEY (`fk_tipoProducto`) REFERENCES `sig_tipoproducto` (`pk_tipoproducto`),
  CONSTRAINT `fk_pro_unidad` FOREIGN KEY (`fk_unidadMedida`) REFERENCES `sig_unidadmedida` (`pk_unidadmedida`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_producto: ~1 rows (aproximadamente)
DELETE FROM `sig_producto`;
/*!40000 ALTER TABLE `sig_producto` DISABLE KEYS */;
INSERT INTO `sig_producto` (`pk_producto`, `nombre`, `descrip`, `fk_unidadMedida`, `presentacion`, `fk_tipoProducto`, `fk_rubro`, `estado`) VALUES
	('1', 'prueba', 'prueba', 1, 'prueba', 1, 150, 1),
	('150000000000001', 'PRUEBA', 'PRUEBA', 1, 'PRUEBA', 1, 150, 1),
	('150000000000002', 'PRUEBA', 'PRUEBA', 1, 'PRUEBA', 1, 150, 1);
/*!40000 ALTER TABLE `sig_producto` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_rol
DROP TABLE IF EXISTS `sig_rol`;
CREATE TABLE IF NOT EXISTS `sig_rol` (
  `pk_rol` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`pk_rol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_rol: ~5 rows (aproximadamente)
DELETE FROM `sig_rol`;
/*!40000 ALTER TABLE `sig_rol` DISABLE KEYS */;
INSERT INTO `sig_rol` (`pk_rol`, `descripcion`, `estado`) VALUES
	(1, 'ROOT', 1),
	(2, 'ADMINISTRADOR', 1),
	(3, 'TACTICO', 1),
	(4, 'ESTRATEGICO', 1),
	(5, 'LABORATORISTA', 1),
	(6, 'TACTICO', 1);
/*!40000 ALTER TABLE `sig_rol` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_rubro
DROP TABLE IF EXISTS `sig_rubro`;
CREATE TABLE IF NOT EXISTS `sig_rubro` (
  `pk_Rubro` int(7) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estado` bit(1) NOT NULL,
  PRIMARY KEY (`pk_Rubro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_rubro: ~0 rows (aproximadamente)
DELETE FROM `sig_rubro`;
/*!40000 ALTER TABLE `sig_rubro` DISABLE KEYS */;
INSERT INTO `sig_rubro` (`pk_Rubro`, `descripcion`, `estado`) VALUES
	(150, 'Prueba', b'1');
/*!40000 ALTER TABLE `sig_rubro` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_salida_bodega
DROP TABLE IF EXISTS `sig_salida_bodega`;
CREATE TABLE IF NOT EXISTS `sig_salida_bodega` (
  `pk_Salida_Bodega` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL,
  `entregado_a` varchar(50) NOT NULL,
  `fecha_auditoria` varchar(30) NOT NULL,
  `observacion` varchar(50) DEFAULT NULL,
  `realizado_por` varchar(50) NOT NULL,
  `fk_Existencia_Bodega` int(11) NOT NULL,
  `fecha_salida` datetime NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `fk_IngresoBodega` varchar(50) DEFAULT NULL,
  `fk_producto` varchar(19) NOT NULL,
  `fk_centro` int(11) NOT NULL,
  PRIMARY KEY (`pk_Salida_Bodega`),
  KEY `FK_salidaB_ExistenciaB` (`fk_Existencia_Bodega`),
  CONSTRAINT `FK_salidaB_ExistenciaB` FOREIGN KEY (`fk_Existencia_Bodega`) REFERENCES `sig_existencia_bodega` (`pk_Existencia_Bodega`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_salida_bodega: ~0 rows (aproximadamente)
DELETE FROM `sig_salida_bodega`;
/*!40000 ALTER TABLE `sig_salida_bodega` DISABLE KEYS */;
INSERT INTO `sig_salida_bodega` (`pk_Salida_Bodega`, `cantidad`, `entregado_a`, `fecha_auditoria`, `observacion`, `realizado_por`, `fk_Existencia_Bodega`, `fecha_salida`, `descripcion`, `fk_IngresoBodega`, `fk_producto`, `fk_centro`) VALUES
	(10, 10, '11', 'Wed Jul 01 00:36:50 CST 2020', 'SIN OBSERVACION', 'SUPER USUARIO', 85, '2020-07-01 00:00:00', NULL, NULL, '150000000000001', 1);
/*!40000 ALTER TABLE `sig_salida_bodega` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_tabulador
DROP TABLE IF EXISTS `sig_tabulador`;
CREATE TABLE IF NOT EXISTS `sig_tabulador` (
  `pk_TABULADOR` int(11) NOT NULL AUTO_INCREMENT,
  `FECHA` date DEFAULT NULL,
  `fk_BENEFICIARIO` int(11) NOT NULL,
  `fk_TCOMIDA` int(11) NOT NULL,
  `fk_Centro` int(11) NOT NULL,
  `TOTAL` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk_TABULADOR`),
  KEY `FK_tabulador_beneficiario` (`fk_BENEFICIARIO`),
  KEY `FK_tabulador_tcomida` (`fk_TCOMIDA`),
  KEY `FK_tabulador` (`fk_Centro`),
  CONSTRAINT `FK_tabulador_beneficiariosig115` FOREIGN KEY (`fk_BENEFICIARIO`) REFERENCES `sig_beneficiario` (`PK_beneficiario`),
  CONSTRAINT `FK_tabulador_centro` FOREIGN KEY (`fk_Centro`) REFERENCES `sig_centro` (`pk_centro`),
  CONSTRAINT `FK_tabulador_tcomida` FOREIGN KEY (`fk_TCOMIDA`) REFERENCES `sig_tcomida` (`pk_TCOMIDA`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_tabulador: ~8 rows (aproximadamente)
DELETE FROM `sig_tabulador`;
/*!40000 ALTER TABLE `sig_tabulador` DISABLE KEYS */;
INSERT INTO `sig_tabulador` (`pk_TABULADOR`, `FECHA`, `fk_BENEFICIARIO`, `fk_TCOMIDA`, `fk_Centro`, `TOTAL`) VALUES
	(182, '2020-07-02', 38, 176, 1, 36),
	(183, '2020-07-02', 39, 177, 1, 36),
	(184, '2020-07-02', 40, 178, 1, 36),
	(185, '2020-07-02', 41, 179, 1, 36),
	(186, '2020-07-03', 42, 180, 1, 36),
	(187, '2020-07-03', 43, 181, 1, 36),
	(188, '2020-07-03', 44, 182, 1, 36),
	(189, '2020-07-03', 45, 183, 1, 36);
/*!40000 ALTER TABLE `sig_tabulador` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_tcomida
DROP TABLE IF EXISTS `sig_tcomida`;
CREATE TABLE IF NOT EXISTS `sig_tcomida` (
  `pk_TCOMIDA` int(11) NOT NULL AUTO_INCREMENT,
  `ALMUERZO` bit(1) DEFAULT NULL,
  `CENA` bit(1) DEFAULT NULL,
  `DESAYUNO` bit(1) DEFAULT NULL,
  `REFRIGERIO` bit(1) DEFAULT NULL,
  PRIMARY KEY (`pk_TCOMIDA`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_tcomida: ~8 rows (aproximadamente)
DELETE FROM `sig_tcomida`;
/*!40000 ALTER TABLE `sig_tcomida` DISABLE KEYS */;
INSERT INTO `sig_tcomida` (`pk_TCOMIDA`, `ALMUERZO`, `CENA`, `DESAYUNO`, `REFRIGERIO`) VALUES
	(176, NULL, NULL, b'1', NULL),
	(177, b'1', NULL, NULL, NULL),
	(178, NULL, b'1', NULL, NULL),
	(179, NULL, NULL, NULL, b'1'),
	(180, NULL, NULL, b'1', NULL),
	(181, b'1', NULL, NULL, NULL),
	(182, NULL, b'1', NULL, NULL),
	(183, NULL, NULL, NULL, b'1');
/*!40000 ALTER TABLE `sig_tcomida` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_tipoproducto
DROP TABLE IF EXISTS `sig_tipoproducto`;
CREATE TABLE IF NOT EXISTS `sig_tipoproducto` (
  `pk_tipoproducto` int(11) NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`pk_tipoproducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_tipoproducto: ~0 rows (aproximadamente)
DELETE FROM `sig_tipoproducto`;
/*!40000 ALTER TABLE `sig_tipoproducto` DISABLE KEYS */;
INSERT INTO `sig_tipoproducto` (`pk_tipoproducto`, `nombre`) VALUES
	(1, 'Perecedero');
/*!40000 ALTER TABLE `sig_tipoproducto` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_ubicacion
DROP TABLE IF EXISTS `sig_ubicacion`;
CREATE TABLE IF NOT EXISTS `sig_ubicacion` (
  `pk_ubicacion` int(11) NOT NULL AUTO_INCREMENT,
  `DEPARTAMENTO` varchar(100) NOT NULL,
  `NOM_DEPTO` varchar(100) DEFAULT NULL,
  `MUNICIPIO` varchar(100) NOT NULL,
  `NOM_MUN` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`pk_ubicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_ubicacion: ~262 rows (aproximadamente)
DELETE FROM `sig_ubicacion`;
/*!40000 ALTER TABLE `sig_ubicacion` DISABLE KEYS */;
INSERT INTO `sig_ubicacion` (`pk_ubicacion`, `DEPARTAMENTO`, `NOM_DEPTO`, `MUNICIPIO`, `NOM_MUN`) VALUES
	(1, '1', 'AHUACHAPAN', '1', 'AHUACHAPÁN'),
	(2, '1', 'AHUACHAPAN', '2', 'APANECA'),
	(3, '1', 'AHUACHAPAN', '3', 'ATIQUIZAYA'),
	(4, '1', 'AHUACHAPAN', '4', 'CONCEPCIÓN DE ATACO'),
	(5, '1', 'AHUACHAPAN', '5', 'EL REFUGIO'),
	(6, '1', 'AHUACHAPAN', '6', 'GUAYMANGO'),
	(7, '1', 'AHUACHAPAN', '7', 'JUJUTLA'),
	(8, '1', 'AHUACHAPAN', '8', 'SAN FRANCISCO MENÉNDEZ'),
	(9, '1', 'AHUACHAPAN', '9', 'SAN LORENZO'),
	(10, '1', 'AHUACHAPAN', '10', 'SAN PEDRO PUXTLA'),
	(11, '1', 'AHUACHAPAN', '11', 'TACUBA'),
	(12, '1', 'AHUACHAPAN', '12', 'TURÍN'),
	(13, '2', 'CABAÑAS', '13', 'CINQUERA'),
	(14, '2', 'CABAÑAS', '14', 'DOLORES'),
	(15, '2', 'CABAÑAS', '15', 'GUACOTECTI'),
	(16, '2', 'CABAÑAS', '16', 'ILOBASCO'),
	(17, '2', 'CABAÑAS', '17', 'JUTIAPA'),
	(18, '2', 'CABAÑAS', '18', 'SAN ISIDRO'),
	(19, '2', 'CABAÑAS', '19', 'SENSUNTEPEQUE'),
	(20, '2', 'CABAÑAS', '20', 'TEJUTEPEQUE'),
	(21, '2', 'CABAÑAS', '21', 'VICTORIA'),
	(22, '3', 'CHALATENANGO', '22', 'AGUA CALIENTE'),
	(23, '3', 'CHALATENANGO', '23', 'ARCATAO'),
	(24, '3', 'CHALATENANGO', '24', 'AZACUALPA'),
	(25, '3', 'CHALATENANGO', '25', 'CANCASQUE'),
	(26, '3', 'CHALATENANGO', '26', 'CHALATENANGO'),
	(27, '3', 'CHALATENANGO', '27', 'CITALÁ'),
	(28, '2', 'CHALATENANGO', '28', 'COMALAPA'),
	(29, '3', 'CHALATENANGO', '29', 'CONCEPCIÓN QUEZALTEPEQUE'),
	(30, '3', 'CHALATENANGO', '30', 'DULCE NOMBRE DE MARÍA'),
	(31, '3', 'CHALATENANGO', '31', 'EL CARRIZAL'),
	(32, '3', 'CHALATENANGO', '32', 'EL PARAÍSO'),
	(33, '3', 'CHALATENANGO', '33', 'LA LAGUNA'),
	(34, '2', 'CHALATENANGO', '34', 'LA PALMA'),
	(35, '3', 'CHALATENANGO', '35', 'LA REINA'),
	(36, '3', 'CHALATENANGO', '36', 'LAS FLORES'),
	(37, '3', 'CHALATENANGO', '37', 'LAS VUELTAS'),
	(38, '3', 'CHALATENANGO', '38', 'NOMBRE DE JESÚS'),
	(39, '3', 'CHALATENANGO', '39', 'NUEVA CONCEPCIÓN'),
	(40, '3', 'CHALATENANGO', '40', 'NUEVA TRINIDAD'),
	(41, '3', 'CHALATENANGO', '41', 'OJOS DE AGUA'),
	(42, '3', 'CHALATENANGO', '42', 'POTONICO'),
	(43, '3', 'CHALATENANGO', '43', 'SAN ANTONIO DE LA CRUZ'),
	(44, '3', 'CHALATENANGO', '44', 'SAN ANTONIO LOS RANCHOS'),
	(45, '3', 'CHALATENANGO', '45', 'SAN FERNANDO'),
	(46, '3', 'CHALATENANGO', '46', 'SAN FRANCISCO LEMPA'),
	(47, '3', 'CHALATENANGO', '47', 'SAN FRANCISCO MORAZÁN'),
	(48, '3', 'CHALATENANGO', '48', 'SAN IGNACIO'),
	(49, '3', 'CHALATENANGO', '49', 'SAN ISIDRO LABRADOR'),
	(50, '3', 'CHALATENANGO', '50', 'SAN LUIS DEL CARMEN'),
	(51, '3', 'CHALATENANGO', '51', 'SAN MIGUEL DE MERCEDES'),
	(52, '3', 'CHALATENANGO', '52', 'SAN RAFAEL'),
	(53, '3', 'CHALATENANGO', '53', 'SANTA RITA'),
	(54, '3', 'CHALATENANGO', '54', 'TEJUTLA'),
	(55, '4', 'CUSCATLAN', '55', 'CANDELARIA'),
	(56, '4', 'CUSCATLAN', '56', 'COJUTEPEQUE'),
	(57, '4', 'CUSCATLAN', '57', 'EL CARMEN'),
	(58, '4', 'CUSCATLAN', '58', 'EL ROSARIO'),
	(59, '4', 'CUSCATLAN', '59', 'MONTE SAN JUAN'),
	(60, '4', 'CUSCATLAN', '60', 'ORATORIO DE CONCEPCIÓN'),
	(61, '4', 'CUSCATLAN', '61', 'SAN BARTOLOMÉ PERULAPÍA'),
	(62, '4', 'CUSCATLAN', '62', 'SAN CRISTÓBAL'),
	(63, '4', 'CUSCATLAN', '63', 'SAN JOSÉ GUAYABAL'),
	(64, '4', 'CUSCATLAN', '64', 'SAN PEDRO PERULAPÁN'),
	(65, '4', 'CUSCATLAN', '65', 'SAN RAFAEL CEDROS'),
	(66, '4', 'CUSCATLAN', '66', 'SAN RAMÓN'),
	(67, '4', 'CUSCATLAN', '67', 'SANTA CRUZ ANALQUITO'),
	(68, '4', 'CUSCATLAN', '68', 'SANTA CRUZ MICHAPA'),
	(69, '4', 'CUSCATLAN', '69', 'SUCHITOTO'),
	(70, '4', 'CUSCATLAN', '70', 'TENANCINGO'),
	(71, '5', 'LA LIBERTAD', '71', 'ANTIGUO CUSCATLÁN'),
	(72, '5', 'LA LIBERTAD', '72', 'CHILTIUPÁN'),
	(73, '5', 'LA LIBERTAD', '73', 'CIUDAD ARCE'),
	(74, '5', 'LA LIBERTAD', '74', 'COLÓN'),
	(75, '5', 'LA LIBERTAD', '75', 'COMASAGUA'),
	(76, '5', 'LA LIBERTAD', '76', 'HUIZÚCAR'),
	(77, '5', 'LA LIBERTAD', '77', 'JAYAQUE'),
	(78, '5', 'LA LIBERTAD', '78', 'JICALAPA'),
	(79, '5', 'LA LIBERTAD', '79', 'LA LIBERTAD'),
	(80, '5', 'LA LIBERTAD', '80', 'NUEVO CUSCATLÁN'),
	(81, '5', 'LA LIBERTAD', '81', 'QUEZALTEPEQUE'),
	(82, '5', 'LA LIBERTAD', '82', 'SACACOYO'),
	(83, '5', 'LA LIBERTAD', '83', 'SAN JOSÉ VILLANUEVA'),
	(84, '5', 'LA LIBERTAD', '84', 'SAN JUAN OPICO'),
	(85, '5', 'LA LIBERTAD', '85', 'SAN MATÍAS'),
	(86, '5', 'LA LIBERTAD', '86', 'SAN PABLO TACACHICO'),
	(87, '5', 'LA LIBERTAD', '87', 'SANTA TECLA'),
	(88, '5', 'LA LIBERTAD', '88', 'TALNIQUE'),
	(89, '5', 'LA LIBERTAD', '89', 'TAMANIQUE'),
	(90, '5', 'LA LIBERTAD', '90', 'TEOTEPEQUE'),
	(91, '5', 'LA LIBERTAD', '91', 'TEPECOYO'),
	(92, '5', 'LA LIBERTAD', '92', 'ZARAGOZA'),
	(93, '6', 'LA PAZ', '93', 'CUYULTITÁN'),
	(94, '6', 'LA PAZ', '94', 'EL ROSARIO'),
	(95, '6', 'LA PAZ', '95', 'JERUSALÉN'),
	(96, '6', 'LA PAZ', '96', 'MERCEDES LA CEIBA'),
	(97, '6', 'LA PAZ', '97', 'OLOCUILTA'),
	(98, '6', 'LA PAZ', '98', 'PARAÍSO DE OSORIO'),
	(99, '6', 'LA PAZ', '99', 'SAN ANTONIO MASAHUAT'),
	(100, '6', 'LA PAZ', '100', 'SAN EMIGDIO'),
	(101, '6', 'LA PAZ', '101', 'SAN FRANCISCO CHINAMECA'),
	(102, '6', 'LA PAZ', '102', 'SAN JUAN NONUALCO'),
	(103, '6', 'LA PAZ', '103', 'SAN JUAN TALPA'),
	(104, '6', 'LA PAZ', '104', 'SAN JUAN TEPEZONTES'),
	(105, '6', 'LA PAZ', '105', 'SAN LUIS LA HERRADURA'),
	(106, '6', 'LA PAZ', '106', 'SAN LUIS TALPA'),
	(107, '6', 'LA PAZ', '107', 'SAN MIGUEL TEPEZONTES'),
	(108, '6', 'LA PAZ', '108', 'SAN PEDRO MASAHUAT'),
	(109, '6', 'LA PAZ', '109', 'SAN PEDRO NONUALCO'),
	(110, '6', 'LA PAZ', '110', 'SAN RAFAEL OBRAJUELO'),
	(111, '6', 'LA PAZ', '111', 'SANTA MARÍA OSTUMA'),
	(112, '6', 'LA PAZ', '112', 'SANTIAGO NONUALCO'),
	(113, '6', 'LA PAZ', '113', 'TAPALHUACA'),
	(114, '6', 'LA PAZ', '114', 'ZACATECOLUCA'),
	(115, '7', 'LA UNION', '115', 'ANAMORÓS'),
	(116, '7', 'LA UNION', '116', 'BOLÍVAR'),
	(117, '7', 'LA UNION', '117', 'CONCEPCIÓN DE ORIENTE'),
	(118, '7', 'LA UNION', '118', 'CONCHAGUA'),
	(119, '7', 'LA UNION', '119', 'EL CARMEN'),
	(120, '7', 'LA UNION', '120', 'EL SAUCE'),
	(121, '7', 'LA UNION', '121', 'INTIPUCÁ'),
	(122, '7', 'LA UNION', '122', 'LA UNIÓN'),
	(123, '7', 'LA UNION', '123', 'LISLIQUE'),
	(124, '7', 'LA UNION', '124', 'MEANGUERA DEL GOLFO'),
	(125, '7', 'LA UNION', '125', 'NUEVA ESPARTA'),
	(126, '7', 'LA UNION', '126', 'PASAQUINA'),
	(127, '7', 'LA UNION', '127', 'POLORÓS'),
	(128, '7', 'LA UNION', '128', 'SAN ALEJO'),
	(129, '7', 'LA UNION', '129', 'SAN JOSÉ'),
	(130, '7', 'LA UNION', '130', 'SANTA ROSA DE LIMA'),
	(131, '7', 'LA UNION', '131', 'YAYANTIQUE'),
	(132, '7', 'LA UNION', '132', 'YUCUAIQUÍN'),
	(133, '8', 'MORAZAN', '133', 'ARAMBALA'),
	(134, '8', 'MORAZAN', '134', 'CACAOPERA'),
	(135, '8', 'MORAZAN', '135', 'CHILANGA'),
	(136, '8', 'MORAZAN', '136', 'CORINTO'),
	(137, '8', 'MORAZAN', '137', 'DELICIAS DE CONCEPCIÓN'),
	(138, '8', 'MORAZAN', '138', 'EL DIVISADERO'),
	(139, '8', 'MORAZAN', '139', 'EL ROSARIO'),
	(140, '8', 'MORAZAN', '140', 'GUALOCOCTI'),
	(141, '8', 'MORAZAN', '141', 'GUATAJIAGUA'),
	(142, '8', 'MORAZAN', '142', 'JOATECA'),
	(143, '8', 'MORAZAN', '143', 'JOCOAITIQUE'),
	(144, '8', 'MORAZAN', '144', 'JOCORO'),
	(145, '8', 'MORAZAN', '145', 'LOLOTIQUILLO'),
	(146, '8', 'MORAZAN', '146', 'MEANGUERA'),
	(147, '8', 'MORAZAN', '147', 'OSICALA'),
	(148, '8', 'MORAZAN', '148', 'PERQUÍN'),
	(149, '8', 'MORAZAN', '149', 'SAN CARLOS'),
	(150, '8', 'MORAZAN', '150', 'SAN FERNANDO'),
	(151, '8', 'MORAZAN', '151', 'SAN FRANCISCO GOTERA'),
	(152, '8', 'MORAZAN', '152', 'SAN ISIDRO'),
	(153, '8', 'MORAZAN', '153', 'SAN SIMÓN'),
	(154, '8', 'MORAZAN', '154', 'SENSEMBRA'),
	(155, '8', 'MORAZAN', '155', 'SOCIEDAD'),
	(156, '8', 'MORAZAN', '156', 'TOROLA'),
	(157, '8', 'MORAZAN', '157', 'YAMABAL'),
	(158, '8', 'MORAZAN', '158', 'YOLOAIQUÍN'),
	(159, '9', 'SAN MIGUEL', '159', 'CAROLINA'),
	(160, '9', 'SAN MIGUEL', '160', 'CHAPELTIQUE'),
	(161, '9', 'SAN MIGUEL', '161', 'CHINAMECA'),
	(162, '9', 'SAN MIGUEL', '162', 'CHIRILAGUA'),
	(163, '9', 'SAN MIGUEL', '163', 'CIUDAD BARRIOS'),
	(164, '9', 'SAN MIGUEL', '164', 'COMACARÁN'),
	(165, '9', 'SAN MIGUEL', '165', 'EL TRÁNSITO'),
	(166, '9', 'SAN MIGUEL', '166', 'LOLOTIQUE'),
	(167, '9', 'SAN MIGUEL', '167', 'MONCAGUA'),
	(168, '9', 'SAN MIGUEL', '168', 'NUEVA GUADALUPE'),
	(169, '9', 'SAN MIGUEL', '169', 'NUEVO EDÉN DE SAN JUAN'),
	(170, '9', 'SAN MIGUEL', '170', 'QUELEPA'),
	(171, '9', 'SAN MIGUEL', '171', 'SAN ANTONIO'),
	(172, '9', 'SAN MIGUEL', '172', 'SAN GERARDO'),
	(173, '9', 'SAN MIGUEL', '173', 'SAN JORGE'),
	(174, '9', 'SAN MIGUEL', '174', 'SAN LUIS DE LA REINA'),
	(175, '9', 'SAN MIGUEL', '175', 'SAN MIGUEL'),
	(176, '9', 'SAN MIGUEL', '176', 'SAN RAFAEL ORIENTE'),
	(177, '9', 'SAN MIGUEL', '177', 'SESORI'),
	(178, '9', 'SAN MIGUEL', '178', 'ULUAZAPA'),
	(179, '10', 'SAN SALVADOR', '179', 'AGUILARES'),
	(180, '10', 'SAN SALVADOR', '180', 'APOPA'),
	(181, '10', 'SAN SALVADOR', '181', 'AYUTUXTEPEQUE'),
	(182, '10', 'SAN SALVADOR', '182', 'CIUDAD DELGADO'),
	(183, '10', 'SAN SALVADOR', '183', 'CUSCATANCINGO'),
	(184, '10', 'SAN SALVADOR', '184', 'EL PAISNAL'),
	(185, '10', 'SAN SALVADOR', '185', 'GUAZAPA'),
	(186, '10', 'SAN SALVADOR', '186', 'ILOPANGO'),
	(187, '10', 'SAN SALVADOR', '187', 'MEJICANOS'),
	(188, '10', 'SAN SALVADOR', '188', 'NEJAPA'),
	(189, '10', 'SAN SALVADOR', '189', 'PANCHIMALCO'),
	(190, '10', 'SAN SALVADOR', '190', 'ROSARIO DE MORA'),
	(191, '10', 'SAN SALVADOR', '191', 'SAN MARCOS'),
	(192, '10', 'SAN SALVADOR', '192', 'SAN MARTÍN'),
	(193, '10', 'SAN SALVADOR', '193', 'SAN SALVADOR'),
	(194, '10', 'SAN SALVADOR', '194', 'SANTIAGO TEXACUANGOS'),
	(195, '10', 'SAN SALVADOR', '195', 'SANTO TOMÁS'),
	(196, '10', 'SAN SALVADOR', '196', 'SOYAPANGO'),
	(197, '10', 'SAN SALVADOR', '197', 'TONACATEPEQUE'),
	(198, '11', 'SAN VICENTE', '198', 'APASTEPEQUE'),
	(199, '11', 'SAN VICENTE', '199', 'GUADALUPE'),
	(200, '11', 'SAN VICENTE', '200', 'SAN CAYETANO ISTEPEQUE'),
	(201, '11', 'SAN VICENTE', '201', 'SAN ESTEBAN CATARINA'),
	(202, '11', 'SAN VICENTE', '202', 'SAN ILDEFONSO'),
	(203, '11', 'SAN VICENTE', '203', 'SAN LORENZO'),
	(204, '11', 'SAN VICENTE', '204', 'SAN SEBASTIÁN'),
	(205, '11', 'SAN VICENTE', '205', 'SAN VICENTE'),
	(206, '11', 'SAN VICENTE', '206', 'SANTA CLARA'),
	(207, '11', 'SAN VICENTE', '207', 'SANTO DOMINGO'),
	(208, '11', 'SAN VICENTE', '208', 'TECOLUCA'),
	(209, '11', 'SAN VICENTE', '209', 'TEPETITÁN'),
	(210, '11', 'SAN VICENTE', '210', 'VERAPAZ'),
	(211, '12', 'SANTA ANA', '211', 'CANDELARIA DE LA FRONTERA'),
	(212, '12', 'SANTA ANA', '212', 'CHALCHUAPA'),
	(213, '12', 'SANTA ANA', '213', 'COATEPEQUE'),
	(214, '12', 'SANTA ANA', '214', 'EL CONGO'),
	(215, '12', 'SANTA ANA', '215', 'EL PORVENIR'),
	(216, '12', 'SANTA ANA', '216', 'MASAHUAT'),
	(217, '12', 'SANTA ANA', '217', 'METAPÁN'),
	(218, '12', 'SANTA ANA', '218', 'SAN ANTONIO PAJONAL'),
	(219, '12', 'SANTA ANA', '219', 'SAN SEBASTIÁN SALITRILLO'),
	(220, '12', 'SANTA ANA', '220', 'SANTA ANA'),
	(221, '12', 'SANTA ANA', '221', 'SANTA ROSA GUACHIPILÍN'),
	(222, '12', 'SANTA ANA', '222', 'SANTIAGO DE LA FRONTERA'),
	(223, '12', 'SANTA ANA', '223', 'TEXISTEPEQUE'),
	(224, '13', 'SONSONATE', '224', 'ACAJUTLA'),
	(225, '13', 'SONSONATE', '225', 'ARMENIA'),
	(226, '13', 'SONSONATE', '226', 'CALUCO'),
	(227, '13', 'SONSONATE', '227', 'CUISNAHUAT'),
	(228, '13', 'SONSONATE', '228', 'IZALCO'),
	(229, '13', 'SONSONATE', '229', 'JUAYÚA'),
	(230, '13', 'SONSONATE', '230', 'NAHUIZALCO'),
	(231, '13', 'SONSONATE', '231', 'NAHUILINGO'),
	(232, '13', 'SONSONATE', '232', 'SALCOATITÁN'),
	(233, '13', 'SONSONATE', '233', 'SAN ANTONIO DEL MONTE'),
	(234, '13', 'SONSONATE', '234', 'SAN JULIÁN'),
	(235, '13', 'SONSONATE', '235', 'SANTA CATARINA MASAHUAT'),
	(236, '13', 'SONSONATE', '236', 'SANTA ISABEL ISHUATÁN'),
	(237, '13', 'SONSONATE', '237', 'SANTO DOMINGO DE GUZMÁN'),
	(238, '13', 'SONSONATE', '238', 'SONSONATE'),
	(239, '13', 'SONSONATE', '239', 'SONZACATE'),
	(240, '14', 'USULUTAN', '240', 'ALEGRÍA'),
	(241, '14', 'USULUTAN', '241', 'BERLÍN'),
	(242, '14', 'USULUTAN', '242', 'CALIFORNIA'),
	(243, '14', 'USULUTAN', '243', 'CONCEPCIÓN BATRES'),
	(244, '14', 'USULUTAN', '244', 'EL TRIUNFO'),
	(245, '14', 'USULUTAN', '245', 'EREGUAYQUÍN'),
	(246, '14', 'USULUTAN', '246', 'ESTANZUELAS'),
	(247, '14', 'USULUTAN', '247', 'JIQUILISCO'),
	(248, '14', 'USULUTAN', '248', 'JUCUAPA'),
	(249, '14', 'USULUTAN', '249', 'JUCUARÁN'),
	(250, '14', 'USULUTAN', '250', 'MERCEDES UMAÑA'),
	(251, '14', 'USULUTAN', '251', 'NUEVA GRANADA'),
	(252, '14', 'USULUTAN', '252', 'OZATLÁN'),
	(253, '14', 'USULUTAN', '253', 'PUERTO EL TRIUNFO'),
	(254, '14', 'USULUTAN', '254', 'SAN AGUSTÍN'),
	(255, '14', 'USULUTAN', '255', 'SAN BUENAVENTURA'),
	(256, '14', 'USULUTAN', '256', 'SAN DIONISIO'),
	(257, '14', 'USULUTAN', '257', 'SAN FRANCISCO JAVIER'),
	(258, '14', 'USULUTAN', '258', 'SANTA ELENA'),
	(259, '14', 'USULUTAN', '259', 'SANTA MARÍA'),
	(260, '14', 'USULUTAN', '260', 'SANTIAGO DE MARÍA'),
	(261, '14', 'USULUTAN', '261', 'TECAPÁN'),
	(262, '14', 'USULUTAN', '262', 'USULUTÁN');
/*!40000 ALTER TABLE `sig_ubicacion` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_unidadmedida
DROP TABLE IF EXISTS `sig_unidadmedida`;
CREATE TABLE IF NOT EXISTS `sig_unidadmedida` (
  `pk_unidadmedida` int(11) NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`pk_unidadmedida`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_unidadmedida: ~0 rows (aproximadamente)
DELETE FROM `sig_unidadmedida`;
/*!40000 ALTER TABLE `sig_unidadmedida` DISABLE KEYS */;
INSERT INTO `sig_unidadmedida` (`pk_unidadmedida`, `nombre`) VALUES
	(1, 'Prueba');
/*!40000 ALTER TABLE `sig_unidadmedida` ENABLE KEYS */;

-- Volcando estructura para tabla sig115.sig_usuario
DROP TABLE IF EXISTS `sig_usuario`;
CREATE TABLE IF NOT EXISTS `sig_usuario` (
  `pk_usuario` varchar(10) NOT NULL,
  `clave` varchar(250) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fk_rol` int(11) NOT NULL,
  `fk_centro` int(11) NOT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `nombre` varchar(70) NOT NULL,
  `apellido` varchar(70) NOT NULL,
  PRIMARY KEY (`pk_usuario`),
  KEY `fk_usu_rol` (`fk_rol`),
  KEY `fk_usu_centro` (`fk_centro`),
  CONSTRAINT `fk_usu_centro` FOREIGN KEY (`fk_centro`) REFERENCES `sig_centro` (`pk_centro`),
  CONSTRAINT `fk_usu_rol` FOREIGN KEY (`fk_rol`) REFERENCES `sig_rol` (`pk_rol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sig115.sig_usuario: ~5 rows (aproximadamente)
DELETE FROM `sig_usuario`;
/*!40000 ALTER TABLE `sig_usuario` DISABLE KEYS */;
INSERT INTO `sig_usuario` (`pk_usuario`, `clave`, `estado`, `fk_rol`, `fk_centro`, `correo`, `nombre`, `apellido`) VALUES
	('AA20001', 'e26c062fedf6b32834e4de93f9c8b644', 1, 5, 1, 'alexislopez350@gmail.com', 'AA', 'AA'),
	('AA20002', 'e26c062fedf6b32834e4de93f9c8b644', 1, 4, 1, 'alexislopez350@gmail.com', 'A', 'A'),
	('AB20001', 'e26c062fedf6b32834e4de93f9c8b644', 1, 2, 1, 'alexislopez350@gmail.com', 'AA', 'BB'),
	('ROOT', '21232f297a57a5a743894a0e4a801fc3', 1, 2, 1, NULL, 'SUPER', 'USUARIO'),
	('YA20001', 'e26c062fedf6b32834e4de93f9c8b644', 1, 3, 1, '', 'YY', 'AA');
/*!40000 ALTER TABLE `sig_usuario` ENABLE KEYS */;

-- Volcando estructura para disparador sig115.audi2
DROP TRIGGER IF EXISTS `audi2`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `audi2` AFTER INSERT ON `sig_descargo_bodega` FOR EACH ROW update sig_existencia_bodega
set cantidad = cantidad - new.cantidad where pk_Existencia_Bodega = new.fk_Existencia_Bodega//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Volcando estructura para disparador sig115.audi3
DROP TRIGGER IF EXISTS `audi3`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `audi3` AFTER INSERT ON `sig_salida_bodega` FOR EACH ROW update sig_existencia_bodega
set cantidad = cantidad - new.cantidad where pk_Existencia_Bodega = new.fk_Existencia_Bodega//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
